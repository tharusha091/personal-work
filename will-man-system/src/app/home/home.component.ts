import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  imageSources = [
    "contract.jpg", 
    "2.jpg",
    "3.jpg",
    "4.jpg",
    "5.jpg",

  ]
  constructor() { }

  ngOnInit() {
  }

}
