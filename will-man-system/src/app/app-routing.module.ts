import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserManagmentComponent }      from './user-managment/user-managment.component';
import { WalletsComponent } from './wallets/wallets.component';
import { ContractsComponent } from './contracts/contracts.component';
import { AssetsComponent } from './assets/assets.component';
import { FriendComponent } from './friend/friend.component';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  // { path: '', component: AppComponent }, 
  { path: 'home', component: HomeComponent }, 
  { path: '', component: UserManagmentComponent }, 
  { path: 'login', component: UserManagmentComponent }, 
  { path: 'contracts', component:  ContractsComponent},
  { path: 'assets', component: AssetsComponent },
  { path: 'friends', component: FriendComponent },
  { path: 'wallets', component: WalletsComponent },

];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ],
})
export class AppRoutingModule {}