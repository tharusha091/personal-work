import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-friend',
  templateUrl: './friend.component.html',
  styleUrls: ['./friend.component.css']
})
export class FriendComponent implements OnInit {
  friendName = '';
  currentUser = '';
  username = '';
  profilePicturePath = '';
  email = '';
  emailVerified = false;
  emailVerificationID = '';
  accountDeactivated = '';
  friendList = [];
  lastLoggedInTime = '';
  isLoggedOut = true;
  
  constructor() { }

  ngOnInit() {
    this.friendName = '';
    
  }

  search(friendName: string) {
    if(friendName) {
      this.friendName = friendName;
    }
    console.log('Friend to search ', this.friendName);
    const username = {
      "username": this.friendName
    }
    const options = {
      method: 'POST',
      body: JSON.stringify(username),
      headers: {
        'Content-Type': 'application/json'
      }
    };

    fetch('http://104.248.186.187:9000/getUserProfile', options)
    .then(response => response.json())
    .then(response => {
      this.currentUser = response;
      this.username = response.data.username;
      this.email = response.data.email; 
      this.emailVerified = response.data.email_verified.status; 
      this.emailVerificationID = response.data.email_verified.verification_id; 
      this.accountDeactivated = response.data.user_account_deactivated ? "Deactive" : "Active";
      this.friendList = response.data.friend_list;
      this.lastLoggedInTime = response.data.last_time_login;
      this.profilePicturePath = response.data.profile_picture.path;
      console.log(response) // Prints result from `response.json()` in getRequest
    })
    .catch(error => console.error(error))//TODO alert
    this.friendName = '';
  }

}
