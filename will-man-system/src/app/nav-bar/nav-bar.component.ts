import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../_services';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  loginSuccess = false;
  loginUrl = 'login';

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
) {   
    // redirect to home if already logged in
    if (this.authenticationService.currentUserValue) { 
        this.loginSuccess =  this.authenticationService.loginSuccess;
        console.log("NavBarComponent ",this.loginSuccess);
    }
  }

  logout() {
    console.log("logout");
    if (this.authenticationService.currentUserValue) {
      this.loginSuccess =  this.authenticationService.loginSuccess;
      console.log("logout", this.loginSuccess);
      var obj = {
        "username": this.authenticationService.currentUserValue.username,
      };
      this.authenticationService.logout(obj);
      this.authenticationService.logout(obj)
          .pipe(first())
          .subscribe(
              data => {
                  this.loginSuccess = this.authenticationService.loginSuccess;
                  this.router.navigateByUrl(this.loginUrl);
              },
              error => {
                console.log("logout error");

                  //this.alertService.error(error);
              });
    }
  }

  ngOnInit() {
  }

}
