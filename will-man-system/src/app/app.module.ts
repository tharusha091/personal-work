import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserManagmentComponent } from './user-managment/user-managment.component';
import { MatButtonModule, MatToolbarModule, MatMenuModule, MatCardModule, MatCardContent, MatFormFieldModule, MatOptionModule, MatSelect, MatSelectModule, MatIconModule, MatInputModule, MatListItemBase, MatListModule, MatBadgeModule } from '@angular/material';
import { FriendComponent } from './friend/friend.component';
import { WalletsComponent } from './wallets/wallets.component';
import { AssetsComponent } from './assets/assets.component';
import { ContractsComponent } from './contracts/contracts.component';
import { AlertComponent } from './_components/alert.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import {SlideshowModule} from 'ng-simple-slideshow';


@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    UserManagmentComponent,
    FriendComponent,
    WalletsComponent,
    AssetsComponent,
    ContractsComponent,
    AlertComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatCardModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatListModule,
    MatSelectModule,
    MatBadgeModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule, 
    SlideshowModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
