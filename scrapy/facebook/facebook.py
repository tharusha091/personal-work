from bs4 import BeautifulSoup
import json
from selenium import webdriver
import sys
import csv
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.common.keys import Keys
import random
from time import sleep
import requests
import re
import sys
from selenium.webdriver.common.keys import Keys


usr = "XXXXXXX"
pwd = "XXXXXX"

url = sys.argv[1]
co = sys.argv[2]


def loginfacebook(driver):
    elem = driver.find_element_by_id("email")
    elem.send_keys(usr)
    elem = driver.find_element_by_id("pass")
    elem.send_keys(pwd)
    elem.send_keys(Keys.RETURN)


def getPageEnd(driver, endText):

    while True:
        ht = driver.find_element_by_tag_name('html')
        ht.send_keys(Keys.END)
        print(endText)
        sleep(1)

        if endText in driver.page_source:
            break
        else:
            continue


def getElementsByClass(tag, className, html):
    soup = BeautifulSoup(html, 'html.parser')
    return soup.find_all(tag, class_=className)


def getValuesByTag(tag, all):
    elementList = []
    for single in all:
        link = single.find('a').attrs[tag]
        elementList.append(link)
    return elementList


def getPageEndByCount(driver, count, name):

    c = int(co)
    if c == 0:
        num = count - 100
    else:
        num = c

    while True:
        ht = driver.find_element_by_tag_name('html')
        ht.send_keys(Keys.END)
        #print(endText)

        text = driver.page_source

        t = text.count(name)
        print (t)
        if (t > num):
            break
        else:
            continue

    ht.send_keys(Keys.HOME)


def getDetails(text, tag, className):
    soup = BeautifulSoup(text, "html.parser")
    s1 = soup.find_all(tag, class_=className)
    if s1 != None and len(s1) > 0:
        return s1[0].contents[0]
    else:
        return None


def getFriendsListInAccount(driver, url):

    driver.get(url)
    sleep(3)
    count = getFriendCount(driver)

    if count == None:
        print("no friends to add")
        return

    print(count)

    name = getuserName(url)

    link = 'https://www.facebook.com/' + name + '/friends'

    driver.get(link)
    sleep(1)

    #driver.find_element_by_link_text("Friends").click()
    #sleep(1)
    #getPageEnd(driver, 'More About')
    getPageEndByCount(driver, count, 'fsl fwb fcb')

    elements = getElementsByClass('div', 'fsl fwb fcb', driver.page_source)
    friends = getValuesByTag('href', elements)
    print('friends count is %s' % len(friends))
    return friends


def getFriendCount(driver):

    result = re.search('_39g5">([0-9,]+)</a>', driver.page_source)
    value = result.group(1)

    if ',' in value:
        value = value.replace(',', '')

    if result == None:
        return None
    else:
        return int(value)


def addFriendsSinglePage(driver, url):

    driver.get(url)
    sleep(3)
    count = getFriendCount(driver)

    if count == None:
        print("no friends to add")
        return

    print(count)

    link = 'https://www.facebook.com/' + getuserName(url) + '/friends'

    driver.get(link)
    sleep(1)

    getPageEndByCount(driver, count, 'fsl fwb fcb')

    addFriendButton = driver.find_elements_by_class_name("FriendRequestAdd")
    clickAddAsFriendButton(driver, addFriendButton)


def addSuggestFriend(driver, url):

    link = 'https://www.facebook.com/find-friends/browser/'

    driver.get(link)
    sleep(1)

    getPageEndByCount(driver, 0, 'fsl fwb fcb')

    addFriendButton = driver.find_elements_by_class_name("FriendRequestAdd")
    
    clickAddAsFriendButton(driver, addFriendButton)


def clickAddAsFriendButton(driver, addFriendButton):
    for bt in addFriendButton:
    
        if 'Add Friend' == bt.text:
            try:
                bt.click()

                sleep(1)
                text = driver.page_source

                if 'Only add people who you know' in text or 'Suggest friends' in text:
                    print('tharusha')
                    buttons = driver.find_elements_by_xpath("//*[contains(text(), 'Close')]")
                elif 'Does this person know you' in text:
                    print('dulan')
                    buttons = driver.find_elements_by_xpath("//*[contains(text(), 'Cancel')]")
                else:
                    print("palliya")
                    continue
                sleep(2)
                for but in buttons:
                    href_data = but.get_attribute('href')
                    if (href_data is not None):
                        but.click()
                        sleep(2)

            except Exception as error:
                sleep(2)
                text = driver.page_source

                if 'Only add people who you know' in text or 'Suggest friends' in text:
                    print('tharusha')
                    buttons = driver.find_elements_by_xpath("//*[contains(text(), 'Close')]")
                elif 'Does this person know you' in text:
                    print('dulan')
                    buttons = driver.find_elements_by_xpath("//*[contains(text(), 'Cancel')]")

                for but in buttons:
                    href_data = but.get_attribute('href')
                    if (href_data is not None):
                        but.click()
                        sleep(2)

                continue


def deleteFbMessages(driver):
    driver.get("https://www.facebook.com/messages")
    sleep(2)
    buttons = driver.find_elements_by_class_name("_5blh")

    sleep(2)
    for but in buttons:
        but.click()
        sleep(2)
        button = driver.find_elements_by_xpath(
            "//*[contains(text(), 'Delete')]")

        button[0].click()
        sleep(2)

        button = driver.find_elements_by_xpath(
            "//*[contains(text(), 'Delete')]")

        for bt in button:
            if (bt.text) == "Delete":
                bt.click()

        sleep(2)


def addFriendByLink(driver, link):
    driver.get(link)
    sleep(2)
    text = driver.page_source
    if 'FriendRequestAdd addButton hidden_elem' in text:
        return
    addFriendButton = driver.find_element_by_class_name("FriendRequestAdd")
    #javascript = "window.scrollTo(0, {0})".format(addFriendButton.location["y"]-300)
    #driver.execute_script(javascript)
    sleep(1)
    if 'Add Friend' == addFriendButton.text:
        addFriendButton.click()


def getuserName(profileURL):
    if profileURL.find('profile.php?id=') == -1:
        if '?' in profileURL:
            y = profileURL.index('?')
        else:
            y = len(profileURL)
        finalProfileName = profileURL[profileURL.index(
            '.com/')+len('.com/'): y]
    else:
        if '&' in profileURL:
            y = profileURL.index('?')
        else:
            y = len(profileURL)
        finalProfileName = profileURL[profileURL.index(
            'profile.php?id=') + len('profile.php?id='):y]
    return finalProfileName


def messageFriendByLink(driver, link, message):
    name = getuserName(link)
    print(name)
    driver.get(
        "https://www.facebook.com/messages/t/{userName}".format(userName=name))
    sleep(2)
    try:
        driver.switch_to_active_element().send_keys(message + Keys.ENTER)
        sleep(2)
    except Exception as error:
        return


def main():

    
   
    try:
        driver = webdriver.Chrome('/path/to/chromedriver')
        driver.get("http://www.facebook.org")
        assert "Facebook" in driver.title
        loginfacebook(driver)
        #flist = getFriendsListInAccount(driver,url)
        a = sys.argv[3]
        if a == '1':
            addFriendsSinglePage(driver, url)
        elif a == '2':
            addSuggestFriend(driver, url)

        driver.quit()
        print('driver is quit')
        exit(0)

    except Exception as error:
        print (error)
        driver.quit()
        print("driver is quit")


if __name__ == "__main__":
    main()
