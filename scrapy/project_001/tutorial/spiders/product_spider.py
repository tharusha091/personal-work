import scrapy
from scrapy.loader.processors import Compose, MapCompose, Join, TakeFirst
from scrapy.loader import ItemLoader 
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import HtmlXPathSelector
from w3lib.html import remove_tags
import sys

def filter_price(value):     #this is used to filter price by removing $ and floating points

    val = value.replace("$","")
    val = val.replace(".","")
    return val


class MySpider(CrawlSpider):
    name = "product"
   
    start_urls = ["https://theurge.com.au/search-results?search-value="]

    rules = (
        Rule(SgmlLinkExtractor(allow=(), ), callback="parse_items", follow= True),
    )


    def parse_items(self, response):
        items = []
        for product in response.css('div.product'):

            loader = ItemLoader(item=Product(),selector=product)     # item load is used to store data
            loader.add_xpath('product_name', "//h2[contains(@class,'product_content_name')]/text()")
            loader.add_xpath('brand', "//span[contains(@class,'product_content_store')]/text()")
            loader.add_xpath('price', "//span[contains(@class,'price_strikethrough')]/text()")
            loader.add_xpath('sale_price', "//span[contains(@class,'price_discount')]/text()")
            productItem = loader.load_item()

            if (productItem not in items):   # to remove duplications in results.
                if len(items) > self.settings.get("MAXIMUM_LIMIT"):    # check maximum scraping limit 
                    print("scraping limit is exeeded")
                    sys.exit()
                
                
                items.append(productItem)
                yield productItem
     

class Product(scrapy.Item):
    product_name = scrapy.Field(
        input_processor = Compose(MapCompose(lambda v: v.strip()), TakeFirst()),
        output_processor = TakeFirst(),
    )
    brand = scrapy.Field(
        input_processor = MapCompose(remove_tags),
        output_processor = TakeFirst(),
    )
    image_link = scrapy.Field(
        input_processor = MapCompose(remove_tags),
        output_processor = TakeFirst(),
    )
    price= scrapy.Field(
        input_processor = MapCompose(remove_tags, filter_price),
        output_processor = TakeFirst(),
    )
    sale_price = scrapy.Field(
        input_processor = MapCompose(remove_tags, filter_price),
        output_processor = Compose(MapCompose(lambda v: v.strip()), TakeFirst()),
    )
    



    
