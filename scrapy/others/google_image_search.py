from selenium import webdriver
import sys
from time import sleep
from bs4 import BeautifulSoup
import csv


def searchfile(writer, filePath, fileName):

    browser = webdriver.Firefox()
    browser.get('http://www.google.com.au/imghp')

    # Click "Search by image" icon
    elem = browser.find_element_by_class_name('FiqGxd')
    elem.click()

    # Switch from "Paste image URL" to "Upload an image"
    browser.execute_script("google.qb.ti(true);return false")

    # Set the path of the local file and submit
    ele0 = browser.find_element_by_id("qbfile")
    ele0.send_keys(filePath)
    
    while("Visually similar images" not in browser.page_source):
        sleep(1)    

    l = browser.page_source

    soup = BeautifulSoup(l, "html.parser")
    res = soup.find_all( "div", class_ = 'r5a77d')
    l = ""

    bestKey = res[0].text
    bestKeyLink = res[0].find('a').attrs['href']

    writer.writerow({'file name': fileName, 'Best guess for this image': bestKey,
                             'Best guess for this image url': bestKeyLink, 'title': "", 'url':  "", 'key words': ""})

    tot = soup.find_all( "div", class_ = 'bkWMgd')
    
    for t in tot:
        text = t.text
        if "Visually similar images" not in text:
            result = t.find_all( "div", class_ = 'g')
            for re in result:
                line = re.text.lower()
                if "shirt" in line or "design" in line or "template" in line or "dress" in line:
                    check = re.find_all("h3", class_ = "LC20lb")

                    link = re.find_all("div", class_ = "r")
                    url = link[0].find('a').attrs['href']
                    tt = re.find_all("span", class_ = "st")
                    tag = tt[0].find_all('em')
                    tit = check[0].text.replace(",","")
                    
                    tags = ""
                    for keyTag in tag:
                        if tags != "":
                            tags = tags + " | "    
                        tags = tags + keyTag.text
                        
                    tagLine = tags.replace(",","")
                    writer.writerow({'file name': "", 'Best guess for this image': "",
                             'Best guess for this image url': "", 'title': tit, 'url':  url, 'key words': tagLine})
    
    browser.quit()

def main():
    myFile = open('final.csv', 'w')

    with myFile:
        myFields = ['file name', 'Best guess for this image',
                'Best guess for this image url', 'title', 'url', 'key words']
        writer = csv.DictWriter(myFile, fieldnames=myFields)
        writer.writeheader()

        file = "/home/tharusha/project/own/image/"

        for i in range(0,100):
        
            fileName = "M1F " + str(i) +  ".png"
            filePath = file + fileName
            searchfile(writer, filePath, fileName)


if __name__ == "__main__":
	main()
