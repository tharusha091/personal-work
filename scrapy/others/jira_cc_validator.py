#!/usr/bin/python
#script by tharusha for cc reviewing and checking
from jira import JIRA,JIRAError

import re,sys,os
import threading,argparse,operator

text = ""
tool = {'SCON': 'SCCat', 'SG': 'SGCCat','REFDFE':'RDCCat','FILE':'FileCCat', 'DB':'OCCat', 'RULE':'fill', 'SCRIPT':'fill', 'MANUAL':'no_cc'}
platformProject = ['SYSG', 'MTECH', 'REFD', 'YAAN', 'AUTH', 'EXCH']
password = ''

parser = argparse.ArgumentParser()
parser.add_argument("userName", help="user name to log the jira account")
parser.add_argument("ticketQuery", help="jira query to search ticket")
parser.add_argument("doManualValidation", default= False, help="manual validation can be done without refering the ticket")
parser.add_argument("assingerIfApproved", default="exch_support", help="assigner that ticket need to be assigned to execute a System CC")
   
args = parser.parse_args()

userName = str(args.userName)
ticketQuery = str(args.ticketQuery)
doManualValidation = bool(args.doManualValidation)
assingerIfApproved = str(args.assingerIfApproved)

options = {'server': 'https://jira.millenniumit.com'}
jira = JIRA(options, basic_auth=(userName,password))

def validate(ticket):
	val = True
	
	val = validateField(ticket.siir,operator.ne,None,"please add SIIR",val)
	val = validateField(ticket.fixVersion,operator.ne,None,"please set TBDIR",val) 
	val = validateField(ticket.crReference,operator.ne,None,"please add CR reference",val)	
	val = validateOnType(ticket,val)
	val = validateCcx(ticket,val)

	return val
def validateField(fieldValue,relate,compareValue,comment,result):
	
	if(not(relate(fieldValue, compareValue))):
		updateRejectComment(comment);	
		val = False
	else:
		val = True
	return result & val

def validateCcx(ticket,result):
	val = True
	ccx = ticket.clientRefNo
	if ccx == None or ticket.type == 'CC-Code':
		return result
	attachmentMap = dict()
	
	for attach in ticket.attachment:
		attachmentMap[attach.filename] = attach
	
	toolName = getToolName(ticket)
	
	val = validateField(toolName,operator.ne,None,'please update cc tool AREA in summery',val)	
	
	for word in ccx.split(','):
		ccId = word.strip()
		if(not(ccId in list(attachmentMap))):
			val = False
			updateRejectComment('please attach ' + ccId)
		else:
			val = validateCcxFile(ticket,toolName,attachmentMap[ccId],val)
	return result & val

def getToolName(ticket):
	title = ticket.summery
	source = re.findall(r'\[([A-Z]+)\]',title)
	for word in source:
		if word in list(tool):
			return word
	return None
	
def isPlatformProject(ticket):
	title = ticket.summery
	source = re.findall(r'\[([A-Z]+)\]',title)
	for word in source:
		if word in platformProject:
			return True
	return False
	
def validateCcxFile(ticket,toolName, attachment, result):
	val = True
	lines = attachment.get()
	header = re.search(r'\<CCHeader(.*)\>',lines).group(0).replace(" ","")
	ticketId = re.search(r'AbacusIDs="([A-Za-z0-9-"]+)\"',header).group(1)	
	crReference = re.search(r'IssuerRef="([A-Za-z0-9-"]+)\"',header).group(1)
	
	if toolName != None:
		ccTool = re.search(r'Tool="([A-Za-z"]+)\"',header).group(1)
		val = validateField(ccTool,operator.eq,tool[toolName],attachment.filename + ' cc is not a '+ toolName+ ' cc',val)
	
	#val = validateField(ticketId,operator.eq,ticket.id,'please correct Abacus id in ' +attachment.filename,val)
	try:
		jira.issue(ticketId)
	
	except JIRAError as err:
		val = False
		updateRejectComment('Error in Abacus Id field in ' + attachment.filename + ' - ' + err.text)
	
	if ticket.crReference != None:	
		val = validateField(crReference,operator.eq,ticket.crReference,'please correct IssuerRef in ' +attachment.filename,val)
	
	return result & val

def validateOnType(ticket,result):
	type = ticket.type
	val = True
	
	if(type == 'CC-System'):
		val = validateSystemCc(ticket,val)
	elif(type == 'CC-Code'):
		val = validateCodeCc(ticket,val)
	
	return result & val	

def validateSystemCc(ticket,result):
	val = True
	lab = ticket.label
	
	if isPlatformProject(ticket) == True:
		if(not(('Solution_Customization_Not_Required' in lab ) or ('Solution_Customization_Required' in lab))):
			updateRejectComment('set relevant label for CC-System ticket')
			val = False
		elif ('Solution_Customization_Required' in lab):
			val = validateField(ticket.complexity,operator.ne,None,"complexity should be updated in CC-System ticket",val)
			
	if getToolName(ticket) == 'no_cc':
		val = validateField(ticket.clientRefNo,operator.ne,None,"please add Client Ref No",val)
	
	return result & val

def validateCodeCc(ticket,result):
	val = True
	subType = ticket.subType
	
	val = validateField(subType,operator.ne,None,"sub type should be there in CC-Code ticket",val)
	val = validateField(ticket.clientRefNo,operator.eq,None,"client ref no is not valid for code cc",val)
	
	if isPlatformProject(ticket) == True:
		if (subType.value != 'CC-Code-Optional') :
			val = validateField(ticket.complexity,operator.ne,None,"complexity should be updated in CC-Code ticket except for CC_CODE_OPTIONAL",val)
			
	return result & val

class Ticket:
    def __init__(self, cc):
        self.summery = cc.fields.summary
        self.id = cc.key
        self.siir = cc.fields.customfield_12500
	self.fixVersion = cc.fields.fixVersions
        self.clientRefNo = cc.fields.customfield_11802
	self.crReference = cc.fields.customfield_12300
        self.type = cc.fields.issuetype.name
        self.subType = getSubType(self.type,cc)
	self.complexity = cc.fields.customfield_12301
        self.attachment = cc.fields.attachment
        self.label = cc.fields.labels
	self.reporter = cc.fields.reporter.key
        
		
def main():

	jiraTickets = jira.search_issues(ticketQuery)
	
	global text
	for cc in jiraTickets:
		
		jiraTicket = jira.issue(cc.key)
		print 'Validating ticket ID is '+ cc.key
		print '**********************************'
		#for field_name in jiraTicket.raw['fields']:
    		#	print "Field:", field_name, "Value:", jiraTicket.raw['fields'][field_name]	
		#continue		
 
		ticket = Ticket(jiraTicket)
	
		#for comment in jiraTicket.fields.comment.comments:
		#	comment.delete()		
		
		text = ""
		if validate(ticket):
			text = "initial validations are passed"	+ '\n'
			if doManualValidation == True:
				if manualValidation(ticket,jiraTicket) == True:
					text = 'ticket is approved'
					assigner = getAssigner(ticket,True)
				else:
					text = text + raw_input("pease enter comment to update the ticket why ticket is not approved - ")
					assigner = getAssigner(ticket,False)
			else:
				jira.add_comment(jiraTicket,text)
				continue
		else:
			assigner = getAssigner(ticket,False)
		
		jira.add_comment(jiraTicket,text)
		ticketUpdateThread = myThread(jiraTicket,assigner)   #Sepertae thread is used to update ticket to minimize latency
		ticketUpdateThread.start()
		print text	

def getAssigner(ticket,approved):
	if approved == False:
		return ticket.reporter
	elif ticket.type == 'CC-Code':
		return ticket.reporter
	else:
		return assingerIfApproved
		
def manualValidation(ticket,jiraTicket):
	print 'Ticket summery - '+ticket.summery+ '\033[0m'
	print 'TBDIR          - '+ ticket.fixVersion[0].name
	print 'SIIR           - '+str(ticket.siir)
	print 'Type           - '+ticket.type
	if ticket.type == 'CC-Code':
		print 'Sub type       - '+ticket.subType
	printIfExist('Labels         -',ticket.label)
	printIfExist('Complexity     - ',ticket.complexity)
	print 'Description    - '
	print jiraTicket.fields.description
	print 'Comments       - '
	
	for comment in jiraTicket.fields.comment.comments:
		print comment.body
		print '******************************************************************'

	var = raw_input("Do you approve the ticket? please enter Y or N : (Y - if approved) - ")
	if var == 'Y' or var == 'y':
		return True
	else:
		return False

def printIfExist(text,source):
	if source != None:
		print text+str(source)
		
def getSubType(type, cc):
        
	if type == 'CC-Code':
		return cc.fields.customfield_17301
	else:
		return None
	
class myThread (threading.Thread):
	def __init__(self,ticket,  reporter):
		threading.Thread.__init__(self)
		self.ticket = ticket
		self.reporter = reporter
	
	def run(self):
		self.ticket.update(assignee = self.reporter )
			
def updateRejectComment(comment):
	global text
	text = text + comment + '\n'

if __name__ == "__main__":
	main()



