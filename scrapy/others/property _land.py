from bs4 import BeautifulSoup
import json
import sys,csv
import requests
import re

def getDetails(text,tag,className):
    soup = BeautifulSoup(text, "html.parser")
    s1 = soup.find_all( tag, class_ = className)
    if s1 != None and len(s1) > 0:    
        return s1[0].contents[0]
    else:
        return None

def getPropertyInfo(text,tag,className):
    soup = BeautifulSoup(text, "html.parser")
    s1 = soup.find_all( tag, class_ = className)

    text = ""

    if s1 != None and len(s1) > 0: 
        for s in s1:
            if text != "":
                text = text + ","
            text = text + (str)(s.contents[0])   
        return text
    else:
        return None



myFile = open('data.csv', 'w')  
with myFile:
    myFields = ['url', 'address', 'condition', 'sold price', 'sold date', 'estimate']
    writer = csv.DictWriter(myFile, fieldnames=myFields)    
    writer.writeheader()
    url = 'https://www.zillow.com/homes/recently_sold/Camarillo-CA/51239_rid/6m_days/globalrelevanceex_sort/34.327702,-118.882198,34.075839,-119.125614_rect/11_zm/'

    res = requests.get(url , headers={"User-Agent":"Mozilla/5.0"})

    l = res.text

    soup = BeautifulSoup(l, "html.parser")
    s = soup.find_all( "div", class_ = 'zsg-photo-card-content zsg-aspect-ratio-content')

    #print(s)
    for ref in s:
        link = ref.find('a').attrs['href']
        abLink = 'https://www.zillow.com' + link

        print(abLink)        
        site = requests.get(abLink , headers={"User-Agent":"Mozilla/5.0"})

        link = site.text
        address1 = getDetails(link,'div','zsg-h1 hdp-home-header-st-addr')
        if address1 == None:
            address1 = getDetails(link, 'h1',  'notranslate')

        address2 = getDetails(link,'div','zsg-h2')

        if address2 == None:
            address2 = getDetails(link,'span','zsg-h2 addr_city')            

        name = getDetails(link,'div','zsg-fineprint date-sold') 
        name = getDetails(link,'span','zsg-icon-for-rent for-rent')
        #   addr_bbs
        print (name)

   
   

