from bs4 import BeautifulSoup
import sys
import requests

code = sys.argv[1]

url = 'https://m.directtextbook.com/ourbuybacktable.php?ean='
base = url + code
res = requests.get(base , headers={"User-Agent":"Mozilla/5.0"})

l = res.text
soup = BeautifulSoup(l, "html.parser")
s = soup.find_all( "tr", class_ = 'pricerow')

for ref in s:
    shopName = ref.a.string
    
    y = ref.find_all( "div", class_ = 'totalint')
    price = y[0].text.replace("\xa0",",")
    line = "vendor = " + shopName + ", price = " + price 
    
    print(line)

    
