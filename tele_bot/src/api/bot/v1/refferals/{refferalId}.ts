
import { ServiceFactory } from '../../../../loader/services/service_factory';
import {ENTITIES} from '../../../../loader/common_defs'

const handler = ServiceFactory.getInstance().getServiceHandler(ENTITIES.Refferal);


module.exports = {

    get: function (req, res, next) {
        handler.getOne(req.params.refferalId, res)
    },
    put: function (req, res, next) {
        handler.update(req.params.refferalId, req.body, res)
    },
    delete: function (req, res, next) {
        handler.delete(req.params.refferalId, res)
    }
}
module.exports.get.apiDoc =  {
    "tags": [
        "Refferals"
    ],
    "summary": "Get a Refferal",
    "description": "Gets the details of a single instance of a `Refferal`.",
    "operationId": "getRefferal",
    "parameters": [{
        "name": "refferalId",
        "in": "path",
        "description": "A unique identifier for a `Refferal`",
        "required": true,
        "type": "string"
    }],
    "responses": {
        "200": {
            "description": "Successful response - returns a single `Refferal`.",
            "schema": {
                "$ref": "#/definitions/Refferal"
            }
        },
        "404": {
            "description": "This is generated when the refferal is not found in the system"
        },
        "500": {
            "description": "Internal error"
        }
    }
}

module.exports.put.apiDoc =  {
    "tags": [
        "Refferals"
    ],
    "summary": "Update a Refferal",
    "description": "Updates an existing `Refferal`.",
    "operationId": "updateRefferal",
    "consumes": [
        "application/json"
    ],
    "produces": [
        "application/json"
    ],
    "parameters": [{
            "name": "body",
            "in": "body",
            "description": "Updated `Refferal` information.",
            "required": true,
            "schema": {
                "$ref": "#/definitions/Refferal"
            }
        },
        {
            "name": "refferalId",
            "in": "path",
            "description": "refferal id",
            "required": true,
            "type": "string"
        }
    ],
    "responses": {
        "202": {
            "description": "Successfull. The updated record will be sent back with this response",
            "schema": {
                "$ref": "#/definitions/Refferal"
            }
        },
        "400": {
            "description": "Validation failure",
            "schema": {
                "$ref": "#/definitions/Error"
            }
        },
        "404": {
            "description": "The refferal id is not found in the system. Cannot be updated"
        },
        "409": {
            "description": "The server has detected that there is newer copy of the entity based on the ref_ver. Update is rejected.\nThe record that was found is sent with this response",
            "schema": {
                "$ref": "#/definitions/Refferal"
            }
        },
        "500": {
            "description": "Internal error. Retry later"
        }
    }
}

module.exports.delete.apiDoc =  {
    "tags": [
        "Refferals"
    ],
    "summary": "Delete a Refferal",
    "description": "Deletes an existing `Refferal`.",
    "operationId": "deleteRefferal",
    "produces": [
        "application/json"
    ],
    "parameters": [{
        "name": "refferalId",
        "in": "path",
        "description": "A unique identifier for a `Refferal`.",
        "required": true,
        "type": "string"
    }],
    "responses": {
        "200": {
            "description": "Succesfull. The deleted record will be sent back with this response",
            "schema": {
                "$ref": "#/definitions/Refferal"
            }
        },
        "400": {
            "description": "Validation failures",
            "schema": {
                "$ref": "#/definitions/Error"
            }
        },
        "404": {
            "description": "Refferal id is not found in the system"
        },
        "500": {
            "description": "Internal error"
        }
    }

}

