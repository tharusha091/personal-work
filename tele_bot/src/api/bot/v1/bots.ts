
import { ServiceFactory } from '../../../loader/services/service_factory';

import {ENTITIES} from '../../../loader/common_defs'

const handler = ServiceFactory.getInstance().getServiceHandler(ENTITIES.Bot);


module.exports = {

    get: function (req, res, next) {
        handler.getAll(res)
    },
    post: function (req, res, next) {
        handler.add(req.body, res)
    }
}

module.exports.get.apiDoc = {
    "tags": [
        "Bots"
    ],
    "summary": "List All Bots",
    "description": "Gets a list of all `Bot` entities.",
    "operationId": "getBots",
    "responses": {
        "200": {
            "description": "Successful response - returns an array of `Bot` entities.",
            "schema": {
                "type": "array",
                "items": {
                    "$ref": "#/definitions/Bot"
                }
            }
        }
    }
}

module.exports.post.apiDoc =   {
    "tags": [
        "Bots"
    ],
    "summary": "Create a Bot",
    "description": "Creates a new instance of a `Bot`.",
    "operationId": "createBot",
    "parameters": [{
        "name": "body",
        "in": "body",
        "description": "A new `Bot` to be created.",
        "required": true,
        "schema": {
            "$ref": "#/definitions/Bot"
        }
    }],
    "responses": {
        "201": {
            "description": "Successful response."
        }
    }

}


