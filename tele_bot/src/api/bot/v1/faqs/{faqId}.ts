
import { ServiceFactory } from '../../../../loader/services/service_factory';
import {ENTITIES} from '../../../../loader/common_defs'

const handler = ServiceFactory.getInstance().getServiceHandler(ENTITIES.Faq);


module.exports = {

    get: function (req, res, next) {
        handler.getOne(req.params.faqId, res)
    },
    put: function (req, res, next) {
        handler.update(req.params.faqId, req.body, res)
    },
    delete: function (req, res, next) {
        handler.delete(req.params.faqId, res)
    }
}
module.exports.get.apiDoc =  {
    "tags": [
        "Faqs"
    ],
    "summary": "Get a Faq",
    "description": "Gets the details of a single instance of a `Faq`.",
    "operationId": "getFaq",
    "parameters": [{
        "name": "faqId",
        "in": "path",
        "description": "A unique identifier for a `Faq`",
        "required": true,
        "type": "string"
    }],
    "responses": {
        "200": {
            "description": "Successful response - returns a single `Faq`.",
            "schema": {
                "$ref": "#/definitions/Faq"
            }
        },
        "404": {
            "description": "This is generated when the faq is not found in the system"
        },
        "500": {
            "description": "Internal error"
        }
    }
}

module.exports.put.apiDoc =  {
    "tags": [
        "Faqs"
    ],
    "summary": "Update a Faq",
    "description": "Updates an existing `Faq`.",
    "operationId": "updateFaq",
    "consumes": [
        "application/json"
    ],
    "produces": [
        "application/json"
    ],
    "parameters": [{
            "name": "body",
            "in": "body",
            "description": "Updated `Faq` information.",
            "required": true,
            "schema": {
                "$ref": "#/definitions/Faq"
            }
        },
        {
            "name": "faqId",
            "in": "path",
            "description": "faq id",
            "required": true,
            "type": "string"
        }
    ],
    "responses": {
        "202": {
            "description": "Successfull. The updated record will be sent back with this response",
            "schema": {
                "$ref": "#/definitions/Faq"
            }
        },
        "400": {
            "description": "Validation failure",
            "schema": {
                "$ref": "#/definitions/Error"
            }
        },
        "404": {
            "description": "The faq id is not found in the system. Cannot be updated"
        },
        "409": {
            "description": "The server has detected that there is newer copy of the entity based on the ref_ver. Update is rejected.\nThe record that was found is sent with this response",
            "schema": {
                "$ref": "#/definitions/Faq"
            }
        },
        "500": {
            "description": "Internal error. Retry later"
        }
    }
}

module.exports.delete.apiDoc =  {
    "tags": [
        "Faqs"
    ],
    "summary": "Delete a Faq",
    "description": "Deletes an existing `Faq`.",
    "operationId": "deleteFaq",
    "produces": [
        "application/json"
    ],
    "parameters": [{
        "name": "faqId",
        "in": "path",
        "description": "A unique identifier for a `Faq`.",
        "required": true,
        "type": "string"
    }],
    "responses": {
        "200": {
            "description": "Succesfull. The deleted record will be sent back with this response",
            "schema": {
                "$ref": "#/definitions/Faq"
            }
        },
        "400": {
            "description": "Validation failures",
            "schema": {
                "$ref": "#/definitions/Error"
            }
        },
        "404": {
            "description": "Faq id is not found in the system"
        },
        "500": {
            "description": "Internal error"
        }
    }

}

