
import { ServiceFactory } from '../../../loader/services/service_factory';

import {ENTITIES} from '../../../loader/common_defs'

const handler = ServiceFactory.getInstance().getServiceHandler(ENTITIES.Investment);


module.exports = {

    get: function (req, res, next) {
        handler.getAll(res)
    },
    post: function (req, res, next) {
        handler.add(req.body, res)
    }
}

module.exports.get.apiDoc = {
    "tags": [
        "Investments"
    ],
    "summary": "List All Investments",
    "description": "Gets a list of all `Investment` entities.",
    "operationId": "getInvestments",
    "responses": {
        "200": {
            "description": "Successful response - returns an array of `Investment` entities.",
            "schema": {
                "type": "array",
                "items": {
                    "$ref": "#/definitions/Investment"
                }
            }
        }
    }
}

module.exports.post.apiDoc =   {
    "tags": [
        "Investments"
    ],
    "summary": "Create a Investment",
    "description": "Creates a new instance of a `Investment`.",
    "operationId": "createInvestment",
    "parameters": [{
        "name": "body",
        "in": "body",
        "description": "A new `Investment` to be created.",
        "required": true,
        "schema": {
            "$ref": "#/definitions/Investment"
        }
    }],
    "responses": {
        "201": {
            "description": "Successful response."
        }
    }

}


