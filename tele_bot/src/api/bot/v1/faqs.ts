
import { ServiceFactory } from '../../../loader/services/service_factory';

import {ENTITIES} from '../../../loader/common_defs'

const handler = ServiceFactory.getInstance().getServiceHandler(ENTITIES.Faq);


module.exports = {

    get: function (req, res, next) {
        handler.getAll(res)
    },
    post: function (req, res, next) {
        handler.add(req.body, res)
    }
}
module.exports.get.apiDoc = {
    "tags": [
        "Faqs"
    ],
    "summary": "List All Faqs",
    "description": "Gets a list of all `Faq` entities.",
    "operationId": "getFaqs",
    "produces": [
        "application/json"
    ],
    "responses": {
        "200": {
            "description": "Successful response - returns an array of faqs",
            "schema": {
                "type": "array",
                "items": {
                    "$ref": "#/definitions/Faq"
                }
            }
        },
        "500": {
            "description": "Triggered due to some internal error. "
        }
    }
}

module.exports.post.apiDoc =  {
    "tags": [
        "Faqs"
    ],
    "summary": "Create a Faq",
    "description": "Creates a new instance of a `Faq`.",
    "operationId": "createFaq",
    "parameters": [{
        "name": "body",
        "in": "body",
        "description": "A new `Faq` to be created.",
        "required": true,
        "schema": {
            "$ref": "#/definitions/Faq"
        }
    }],
    "responses": {
        "201": {
            "description": "Success. Newly created record will be sent with this response",
            "schema": {
                "$ref": "#/definitions/Faq"
            }
        },
        "400": {
            "description": "Validation failures",
            "schema": {
                "$ref": "#/definitions/Error"
            }
        },
        "409": {
            "description": "The server has detected another record in database with the same key. That record will be sent with this response",
            "schema": {
                "$ref": "#/definitions/Faq"
            }
        },
        "500": {
            "description": "Inetnal error"
        }
    }
}