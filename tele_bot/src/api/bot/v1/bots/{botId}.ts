
import { ServiceFactory } from '../../../../loader/services/service_factory';

import {ENTITIES} from '../../../../loader/common_defs'

const handler = ServiceFactory.getInstance().getServiceHandler(ENTITIES.Bot);


module.exports = {

    get: function (req, res, next) {
        handler.getOne(req.params.botId, res)
    },
    put: function (req, res, next) {
        handler.update(req.params.botId, req.body, res)
    },
    delete: function (req, res, next) {
        handler.delete(req.params.botId, res)
    }
}

module.exports.get.apiDoc = {
    "tags": [
        "Bots"
    ],
    "summary": "Get a Bot",
    "description": "Gets the details of a single instance of a `Bot`.",
    "operationId": "getBot",
    "responses": {
        "200": {
            "description": "Successful response - returns a single `Bot`.",
            "schema": {
                "$ref": "#/definitions/Bot"
            }
        }
    },
    "parameters": [{
        "name": "botId",
        "in": "path",
        "description": "A unique identifier for a `Bot`.",
        "required": true,
        "type": "string"
    }]
}

module.exports.put.apiDoc =  {
    "tags": [
        "Bots"
    ],
    "summary": "Update a Bot",
    "description": "Updates an existing `Bot`.",
    "operationId": "updateBot",
    "parameters": [{
        "name": "body",
        "in": "body",
        "description": "Updated `Bot` information.",
        "required": true,
        "schema": {
            "$ref": "#/definitions/Bot"
        }
    }],
    
    "responses": {
        "202": {
            "description": "Successful response."
        }
    }
}

module.exports.delete.apiDoc =  {
    "tags": [
        "Bots"
    ],
    "summary": "Delete a Bot",
    "description": "Deletes an existing `Bot`.",
    "operationId": "deleteBot",
    "produces": [
        "application/json"
    ],
    "parameters": [
        {
            "name": "botId",
            "in": "path",
            "description": "A unique identifier for a `Bot`.",
            "required": true,
            "type": "string"
        }
    ],
    "responses": {
        "200": {
            "description": "Successful response. Will return the deleted entity",
            "schema": {
                "$ref": "#/definitions/Bot"
            }
        },
        "400": {
            "description": "Validation failures",
            "schema": {
                "$ref": "#/definitions/Error"
            }
        },
        "404": {
            "description": "Specific Bot id is not found in the system"
        },
        "500": {
            "description": "Internal error"
        }
    }
}