
import { ServiceFactory } from '../../../loader/services/service_factory';

import {ENTITIES} from '../../../loader/common_defs'

const handler = ServiceFactory.getInstance().getServiceHandler(ENTITIES.Refferal);


module.exports = {

    get: function (req, res, next) {
        handler.getAll(res)
    },
    post: function (req, res, next) {
        handler.add(req.body, res)
    }
}
module.exports.get.apiDoc = {
    "tags": [
        "Refferals"
    ],
    "summary": "List All Refferals",
    "description": "Gets a list of all `Refferal` entities.",
    "operationId": "getRefferals",
    "produces": [
        "application/json"
    ],
    "responses": {
        "200": {
            "description": "Successful response - returns an array of refferals",
            "schema": {
                "type": "array",
                "items": {
                    "$ref": "#/definitions/Refferal"
                }
            }
        },
        "500": {
            "description": "Triggered due to some internal error. "
        }
    }
}

module.exports.post.apiDoc =  {
    "tags": [
        "Refferals"
    ],
    "summary": "Create a Refferal",
    "description": "Creates a new instance of a `Refferal`.",
    "operationId": "createRefferal",
    "parameters": [{
        "name": "body",
        "in": "body",
        "description": "A new `Refferal` to be created.",
        "required": true,
        "schema": {
            "$ref": "#/definitions/Refferal"
        }
    }],
    "responses": {
        "201": {
            "description": "Success. Newly created record will be sent with this response",
            "schema": {
                "$ref": "#/definitions/Refferal"
            }
        },
        "400": {
            "description": "Validation failures",
            "schema": {
                "$ref": "#/definitions/Error"
            }
        },
        "409": {
            "description": "The server has detected another record in database with the same key. That record will be sent with this response",
            "schema": {
                "$ref": "#/definitions/Refferal"
            }
        },
        "500": {
            "description": "Inetnal error"
        }
    }
}