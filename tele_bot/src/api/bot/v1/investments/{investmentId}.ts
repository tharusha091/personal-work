
import { ServiceFactory } from '../../../../loader/services/service_factory';

import {ENTITIES} from '../../../../loader/common_defs'

const handler = ServiceFactory.getInstance().getServiceHandler(ENTITIES.Investment);


module.exports = {

    get: function (req, res, next) {
        handler.getOne(req.params.investmentId, res)
    },
    put: function (req, res, next) {
        handler.update(req.params.investmentId, req.body, res)
    },
    delete: function (req, res, next) {
        handler.delete(req.params.investmentId, res)
    }
}

module.exports.get.apiDoc = {
    "tags": [
        "Investments"
    ],
    "summary": "Get a Investment",
    "description": "Gets the details of a single instance of a `Investment`.",
    "operationId": "getInvestment",
    "responses": {
        "200": {
            "description": "Successful response - returns a single `Investment`.",
            "schema": {
                "$ref": "#/definitions/Investment"
            }
        }
    },
    "parameters": [{
        "name": "investmentId",
        "in": "path",
        "description": "A unique identifier for a `Investment`.",
        "required": true,
        "type": "string"
    }]
}

module.exports.put.apiDoc =  {
    "tags": [
        "Investments"
    ],
    "summary": "Update a Investment",
    "description": "Updates an existing `Investment`.",
    "operationId": "updateInvestment",
    "parameters": [{
        "name": "body",
        "in": "body",
        "description": "Updated `Investment` information.",
        "required": true,
        "schema": {
            "$ref": "#/definitions/Investment"
        }
    }],
    
    "responses": {
        "202": {
            "description": "Successful response."
        }
    }
}

module.exports.delete.apiDoc =  {
    "tags": [
        "Investments"
    ],
    "summary": "Delete a Investment",
    "description": "Deletes an existing `Investment`.",
    "operationId": "deleteInvestment",
    "produces": [
        "application/json"
    ],
    "parameters": [
        {
            "name": "investmentId",
            "in": "path",
            "description": "A unique identifier for a `Investment`.",
            "required": true,
            "type": "string"
        }
    ],
    "responses": {
        "200": {
            "description": "Successful response. Will return the deleted entity",
            "schema": {
                "$ref": "#/definitions/Investment"
            }
        },
        "400": {
            "description": "Validation failures",
            "schema": {
                "$ref": "#/definitions/Error"
            }
        },
        "404": {
            "description": "Specific Investment id is not found in the system"
        },
        "500": {
            "description": "Internal error"
        }
    }
}