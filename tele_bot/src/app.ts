

import * as express from "express";
import * as bodyParser from "body-parser";
import { Loader } from "./loader/loader";

import { Logger } from "./common/logger";
const ui=require('swagger-ui-express');
const yaml=require('yamljs');
const config=require('config');
const  open_api = require('express-openapi');

class App{

    public app: express.Application;
   // public routePrv: Routes = new Routes();
    private loader:any
    public api:any=null;
    private server:any=null;

    constructor() {
        
        this.app = express();
        this.loader = Loader.getInstance();
        this.configure();
    }

 

    start(url: string, port: number) {
        return new Promise(async (resolve, reject)=>{
            
            this.loader.init(config.loader, url);
            await this.loader.connect(false);

            this.app.use('/docs', ui.serve, ui.setup(this.api.apiDoc));
            
            this.server = this.app.listen(port, () => {
                this.server.timeout = 300000;
                Logger.getLogger().info('bot bot server listening on port ' + port)
                resolve("OK");
            })
        })
    }
    stop(){
        return new Promise(async (resolve, reject)=>{
            this.loader.disconnect();
            this.server.close((err)=>{
                resolve("OK");// we dont really care about the error here
            });
        })
    }

    private configure(): void { 
          
        // support application/json type post data
        this.app.use(bodyParser.json());
        // support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.get('/', function (req, res) {
            res.status(200).send("Welcome to bot bot api server");
        })
        this.app.use(function(req: any, res: any, next: any){
           Logger.getLogger().trace("** [%s] %s ---- BODY=>%s",req.method, req.originalUrl, JSON.stringify(req.body))
           next();
        })
       
        const spec= './src/api/api-doc.yml';

        //const docs_json = yaml.load(spec); 
        
        this.api = open_api.initialize({
          app: this.app,
          paths: './lib/api',
          apiDoc: spec,
          validateApiDoc: true    
        });

        this.app.use(function(err: any, req: any, res: any, next: any){
            Logger.getLogger().error("API error", JSON.stringify(err));

            res.status(err.status || 500).send({errors: err.message ? err.message : err.errors});
       })
  
    }

}

export default new App();