
import { MongoReader } from './mongo_reader';
import { Logger } from '../../common/logger';
import { DataHandler } from './data_handler';
const loki = require("lokijs");
const MongoClient = require('mongodb').MongoClient;
import {getCollectionName} from '../common_defs'
// const sprintf = require('sprintf');
// const assert = require('assert');

// MongoBot is a direct integration of a bot to a mongodb collection and monitors a single load balanced domain.
export class FileHandler extends DataHandler  {
    
    
  

    constructor(readonly configuration: any, readonly fileContent: any|null, readonly filePath: string){
        super(configuration); 
        this.dataMap = new loki(this.configuration.fileName, this.configuration.loadFile )
    
    }

    loadData(){
        return new Promise((resolve, reject) => {
            try {
                var fileObject = this.fileContent;
                if(fileObject == null)
                    fileObject = require(this.filePath);
                    
                this.configuration.entities.forEach((entity:any)=>{
                    let collectionName = getCollectionName(entity.entity_name);
                    this.insertDataArray(collectionName, fileObject[collectionName])
                });
                return resolve('ok');
            }
            catch (err) {
                Logger.getLogger().error("Caught when attempting to load file ", err);
                return reject(err);
            }
        });

        
    }
    stop() {
        super.stop();
        
    }
  

}