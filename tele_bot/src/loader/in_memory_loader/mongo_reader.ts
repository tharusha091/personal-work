
import { Logger } from '../../common/logger';
import { DynamicUpdate } from './dynamic_update';
var MongoClient = require('mongodb').MongoClient;
export class MongoReader{
    private collectionMap = new Map();
    private db: any;
    private client: any;

    constructor(readonly dburl: string, readonly dynamicUpdate: DynamicUpdate){
          
    }
    
    createConnection(dbURL: string | null = 'mongodb://localhost:27017/bot'){
        if (dbURL === null) {
            return Promise.resolve('Not loaded');
        } 

        return new Promise((resolve, reject) => {
            MongoClient.connect(dbURL, {
                useNewUrlParser: true,
                ignoreUndefined: true
                
            }, async (err: any, client: any) => {
                if (err) {
                    Logger.getLogger().error(' Error connecting to DB URL. Details: ' + err);
                    return reject(err);
                }

                this.db = client.db();
                this.client = client;
                return resolve("ok")
            });
        });    
    }
    watchCollection(collectionName: string){
        Logger.getLogger().trace("watch collection enabled")
        let change_streams = this.db.collection(collectionName).watch();

        change_streams.on('change',  (change) =>{
            Logger.getLogger().debug("watchCollection onChange() - %s",  JSON.stringify(change))
            switch(change.operationType){
                case "insert":
                    this.dynamicUpdate.onInsert(collectionName, change.fullDocument);
                    break;
                case "delete":
                    this.dynamicUpdate.onDelete(collectionName, change.documentKey);
                    break;
                case "update":
                    this.dynamicUpdate.onUpdate(collectionName, change.documentKey, change.updateDescription.updatedFields) 
            }
        });    
    }

    loadData(collectionName: string): Promise<any[]>{
        return new Promise((resolve, reject) => {
            this.getCollection(this.db, collectionName)
            .then((collection: any) => {
                this.collectionMap.set(collectionName, collection);
                return collection.find({}).toArray();

            })
            .then((msg: Array<{}>) => {  
                return resolve(msg)
            })
            .catch((error: any) => {
                Logger.getLogger().error(' Unable to load collection ', collectionName, error);
                return reject(error);

            })

        })
    }

    stop() {
        if (this.client) {
            this.client.close();
            this.client = null;
        }
    }

    getCollection = (db: any, name: string) => {

        return new Promise((resolve, reject) => {
            db.collection(name, { strict: true }, (err: any, collection: any) => {
                if (err) {
                    return reject(`collection not found ${name}`);
                }
                resolve(collection);
            });
        });
    }

}