const Enum = require("enum");

export enum ENTITIES {
    Base = "BASE", // base entity. Not a real entitiy
    Refferal = "REFFERAL",
    Account = "ACCOUNT",
    Investment = "INVESTMENT",
    Bot = "BOT",
    Faq = "FAQ"
   
}

export enum COLLECTIONS {
    Refferal = "bot_refferals",
    Account = "bot_accounts",
    Investment = "bot_investments",
    Bot = "bot_bots",
    Faq = "bot_faqs"
    
}

export enum FIELDS {
    

}

export const getCollectionName = function (entity: ENTITIES): string {
    switch (entity) {

        case ENTITIES.Refferal:
            return COLLECTIONS.Refferal;
        case ENTITIES.Account:
            return COLLECTIONS.Account;
        case ENTITIES.Investment:
            return COLLECTIONS.Investment;
        case ENTITIES.Bot:
            return COLLECTIONS.Bot;
        case ENTITIES.Faq:
            return COLLECTIONS.Faq;
    }
}
