

import * as mongoose from 'mongoose';
import { Request, Response } from 'express';


import { BaseEntity } from './base-service';
import { Loader } from "../loader";
import { ENTITIES, FIELDS } from '../common_defs'


class Faq extends BaseEntity{
    private permissionCheck = false;

    constructor(readonly entityName: ENTITIES, readonly key: string, readonly model: mongoose.Model, readonly schema: mongoose.Schema) {
        super(entityName, key, model, schema);
    }

    public async clean() {
        return super.clean()

    }


    public getAllFaqs() {
        let filter = {};
        return Loader.getInstance().findMany(this.entityName, filter)
    }

    
  

}

export const createFunction = function (entityName: ENTITIES, key: string, model: mongoose.Model, schema: mongoose.Schema) {
    return new Faq(entityName, key, model, schema)
}
export const entityName = ENTITIES.Faq