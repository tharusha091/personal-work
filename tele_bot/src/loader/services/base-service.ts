import { Logger } from '../../common/logger';

import { Request, Response } from 'express';
import {ENTITIES, COLLECTIONS, getCollectionName } from '../common_defs'

import * as mongoose from 'mongoose';
import { Loader} from "../loader";

export class BaseEntity{

    constructor(readonly entityName:ENTITIES, readonly key:string, readonly model:mongoose.Model, readonly schema:mongoose.Schema) {

    }

    copy(src:any, dest:any):void{
        for (let key in src){
            dest[key] = src[key]
        }
    }

    public sanitize(source:any):any{
        // need to strip unwanted meta data from the message
        let tgt = JSON.parse(JSON.stringify(source)) // Note: investgate later. cannot do delete on the source. it doesnt work
        delete tgt._id
        delete tgt.__v
        delete tgt.$loci
        delete tgt.meta
     
        return tgt;
    }

    public handleError(err:any, res:Response):void{
        Logger.getLogger().error('handleError(): ', err);

        if ( err.hasOwnProperty("errors") ){
            let errors = [];

            for (let key in err.errors){
                let subErr = err.errors[key]

                let outErr:any = {}
                outErr.message = subErr.message;
                outErr.path = subErr.path;

                errors.push(outErr);
            }
            
            res.status(400).json({"errors":errors});
        }
        else
           res.status(500).end();
    }

    public add(data:any, res: Response):void {
        Logger.getLogger().trace("Enitity:add() %s", data)

        let doc = this.getDocument(data); 

        Logger.getLogger().debug("add()- Object to be inserted -%s", doc);
        let found:boolean = false;

        let id = data[this.getQueryKey()];

        this.getExistingDoc(id)
        .then(old=>{
            if (old){
                found= true
                return res.status(409).json(this.sanitize(old));
            }
            else
                return doc.save()
        })
        .then(inst=>{
            if (!found){
                Loader.getInstance().inMemoryCache.onInsert(this.getCollectionName(),inst); // update in-memory cache
                res.status(201).json(this.sanitize(inst));
            }
        })
        .catch((err)=>{
            this.handleError(err, res);
        })
    }
    public update(id:string, data: any, res: Response):void {
        Logger.getLogger().trace("Enitity:update() %s", data)

        let error=true;

        this.getExistingDoc(id)
        .then(doc=>{
        
            if (doc == null)
                return res.status(404).end();

            error =false;
            data.ref_ver++;
            data[this.getQueryKey()] = id;
       
            let model = this.getModel()

            return model.findByIdAndUpdate(doc._id, data, {new:true,  runValidators:true}).exec();
        })
        .then(inst=>{
            if (!error){
                Loader.getInstance().inMemoryCache.onUpdate(this.getCollectionName(),  data[this.getQueryKey()], inst); // update in-memory cache
                res.status(202).json(this.sanitize(inst))
            }
        })
        .catch(err=>{
            this.handleError(err, res);
        })
       
    }
    public delete(id:string, res: Response):void {
        Logger.getLogger().trace("Enitity:delete() %s", id)

        let deleted = false;
        this.getExistingDoc(id)
        .then(doc=>{
        
            if (doc == null)
                return res.status(404).end();
       
            let model = this.getModel()
            deleted = true;
            return model.findByIdAndDelete(doc._id ).exec();
        })
        .then(inst=>{
            if (deleted){
                Loader.getInstance().inMemoryCache.onDelete(this.getCollectionName(),  inst[this.getQueryKey()]); // update in-memory cache
                res.status(200).json(this.sanitize(inst))
            }
        })
        .catch(err=>{
            this.handleError(err, res);
        })
    }

    public getAll(res: Response):void {
        Logger.getLogger().trace("Enitity:getAll()")

        let model = this.getModel()

        let docs = Loader.getInstance().findMany(this.entityName,{});
        
        res.status(200).send(docs);
          
    }
    public getOne(id:string, res: Response){
        Logger.getLogger().trace("Enitity:getOne() %s", id)
       
        this.getExistingDoc(id)
        .then(doc=>{
            if (doc === null)
                res.status(404).end()
            else
                res.status(200).json(this.sanitize(doc));
        })
        .catch(err=>{
            this.handleError(err, res);
        })
    }

    // Returns an existing document in the collection. if not found it returns null
    public getExistingDoc(id:string, useCache:boolean=true):any{
        let model = this.getModel()
        let query = {}
        query[this.getQueryKey()] = id;
       
        if (useCache){
            try{
                let doc = Loader.getInstance().findOne(this.entityName,query);
                return Promise.resolve(doc)
            }
            catch(err){
                Logger.getLogger().error("Cache error ", err)
                return Promise.reject(err)
            }
            
        }
        else
            return model.findOne(query).exec();
    }


    public getModel():mongoose.Model|null {
        return this.model;
    }

    protected getCollectionName():string {
        return getCollectionName(this.entityName);
     }

    protected getDocument(data:any):mongoose.Document|null {
        const model = this.getModel();
        let obj = new model;
        this.copy(data, obj);
        
        return obj;
    }
    public getQueryKey():string{
        return this.key;
    }

    protected getQueryWithKey(id: string):any{
        let query = {}
        query[this.getQueryKey()] = id;
        return query;
    }
    
    public async clean(){
        Loader.getInstance().inMemoryCache.removeCollection(this.getCollectionName()); // This will clean the inmemory cache
        return this.getModel().deleteMany({}).exec();
    } 
}

export const createFunction = function(entityName:ENTITIES, key:string, model:mongoose.Model, schema:mongoose.Schema){
    return new BaseEntity(entityName, key, model, schema)
}
export const entityName = ENTITIES.Base; //Default