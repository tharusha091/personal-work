

import * as mongoose from 'mongoose';
import { ENTITIES, COLLECTIONS, getCollectionName } from '../common_defs'
import { validateForiegnKey } from "./schema_validators"

const Schema = mongoose.Schema;



export const botSchema = new Schema({
    level: {
        type: String,
        required: true,
        unique: true
    },
    referalCount: {
        type: String,
        required: false
    },
    internal_name: {
        type: String,
        required: false,
    }
});



export const EntityName = ENTITIES.Refferal
export const Model = mongoose.model(getCollectionName(EntityName), botSchema);
export const EntityKey = "level"
