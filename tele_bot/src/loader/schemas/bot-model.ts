

import * as mongoose from 'mongoose';
import { ENTITIES, COLLECTIONS, getCollectionName } from '../common_defs'
import { validateForiegnKey } from "./schema_validators"

const Schema = mongoose.Schema;



export const botSchema = new Schema({
    bot_id: {
        type: String,
        required: true,
        unique: true,
        maxlength: [20, 'BankID should be less than 30 characters']
    },
    config: {
        type: String,
        required: false
        
    },
    value: {
        type: Number,
        required: false
        
    }
});



export const EntityName = ENTITIES.Bot
export const Model = mongoose.model(getCollectionName(EntityName), botSchema);
export const EntityKey = "bot_id"
