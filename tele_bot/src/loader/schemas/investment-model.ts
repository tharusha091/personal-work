

import * as mongoose from 'mongoose';
import { ENTITIES, COLLECTIONS, getCollectionName } from '../common_defs'
import { validateForiegnKey } from "./schema_validators"

const Schema = mongoose.Schema;



export const botSchema = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
       
    },
    internal_name: {
        type: String,
        required: false
        
    },
    bitcoins: {
        type: String,
        required: false
        
    },
    return: {
        type: String,
        required: false
        
    },
    refund: {
        type: String,
        required: false
        
    }
});



export const EntityName = ENTITIES.Investment
export const Model = mongoose.model(getCollectionName(EntityName), botSchema);
export const EntityKey = "name"
