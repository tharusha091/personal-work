

import * as mongoose from 'mongoose';
import { ENTITIES, COLLECTIONS, getCollectionName } from '../common_defs'
import { validateForiegnKey } from "./schema_validators"

const Schema = mongoose.Schema;



export const botSchema = new Schema({
    faq_id: {
        type: String,
        required: true,
        unique: true
    },
    question: {
        type: String,
        required: false
    },
    answer: {
        type: String,
        required: false,
    }
});



export const EntityName = ENTITIES.Faq
export const Model = mongoose.model(getCollectionName(EntityName), botSchema);
export const EntityKey = "faq_id"
