
import { ENTITIES, COLLECTIONS, getCollectionName } from '../common_defs'
import { validateForiegnKey } from "./schema_validators"
import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const InvestmentSchema = new Schema({
    name:{
        type: String,
        required: false,
    },
    internal_name:{
        type: String,
        required: false,
    },
    bitcoins:{
        type: String,
        required: false,
    }
})

const statObject = new Schema({
    name:{
        type: String,
        required: false,
    },
    internal_name:{
        type: String,
        required: false,
    },
    referalGenerated:{
        type: String,
        required: false,
    },
    referalActivated:{
        type: String,
        required: false,
    },
    totalBot:{
        type: String,
        required: false,
    }
})

const CampaignSchema = new Schema({
    Refferal_link:{
        type: Number,
        required: false,
    },
    Campaign_name:{
        type: Boolean,
        required: false,
    },
    Campaign_link:{
        type: Boolean,
        required: false,
    },
    Stats:{
        type: statObject,
        required: false,
    }
   
})


export const botSchema = new Schema({

    uuid: {
        type: String,
        required: true,
        unique: true
       
    },
    email: {
        type: String,
        
    },
    password: {
        type: Number,
        required: false,
    },
    Bitcoin_wallet : {
        type: String,
        required: false,
    },
    Investment: {
        type: [InvestmentSchema],
        required: false,
    },
    Campaign: {
        type: [CampaignSchema],
        required: false,
    },
    ref_ver:{}
   
});

export const EntityName = ENTITIES.Account
export const Model = mongoose.model(getCollectionName(EntityName), botSchema);
export const EntityKey = "uuid"