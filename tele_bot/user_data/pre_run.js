var MongoClient = require('mongodb').MongoClient,
    test = require('assert');

var mongourl = 'mongodb://localhost:27017';


// node pre_run.js 'mongodb://localhost:27017' 0

const args = process.argv.slice(2);
if (args.length == 1) {
    mongourl = args[0];
} else {
    console.log('Database url is not specified. Defaulting.')
}
var action = 0;
if (args.length == 2) {

    action = args[1];
}



console.log('Using %s', mongourl)

MongoClient.connect(mongourl, { useNewUrlParser: true }, function(err, client) {
    if (err) {
        console.log(" Error connecting to DB URL. Details: " + err);
        process.exit();
    }
    var db = client.db('bot');


    var promises1 = [
        //------------ for cleaning up old tables ---------


        db.dropCollection('bot_accounts'),
        db.dropCollection('bot_refferals'),
        db.dropCollection('bot_faqs'),
        db.dropCollection('bot_investments'),
        db.dropCollection('bot_bots')

    ]

    Promise.all(promises1)
        .then((smg) => {
            console.log(smg)
        })
        .catch((err) => {
            // console.log(err);
        })
        .finally(() => {
            console.log("all collections dropped")

            var promises2 = [



                creatCollection(db, 'bot_accounts'),
                creatCollection(db, 'bot_refferals'),
                creatCollection(db, 'bot_faqs'),
                creatCollection(db, 'bot_investments'),
                creatCollection(db, 'bot_bots')


            ]

            Promise.all(promises2)
                .then((smg) => {
                    console.log(smg)
                })
                .catch((err) => {
                    console.log(err);
                })
                .finally(() => {
                    // ['./bot_accounts.json', './bot_refferals.json', './bot_services.json', './bot_products.json'].forEach((path) => {
                    //     loadData(db, path);
                    // })

                    client.close();
                })
        })
})


async function loadData(db, path) {
    var records = require(path);
    for (let key in records) {
        console.log("Found records for ", key)

        try {
            let collection = await getCollection(db, key)

            if (action == 1)
                await collection.deleteMany({})

            await collection.insertMany(records[key])
            console.log(" %d records successfully inserted", records[key].length)
        } catch (error) {
            console.error("Error: ", error)

        }
    }
}

getCollection = function(db, name) {
    // console.info("accessing collection .......", name);
    return new Promise((resolve, reject) => {
        db.collection(name, { strict: true }, (err, collection) => {
            if (err) {
                return reject('Error: ' + err);
            }
            resolve(collection);
        });
    });
}


var creatCollection = function(db, collection_name, collection_schema = {}, options = {}) {
    return new Promise(function(resolve, reject) {
        //console.log("creating collection ", collection_name, " with schema ", JSON.stringify(collection_schema))
        db.createCollection(collection_name, options, function(err, collection) {

            if (err) {
                console.log("Could not create the collection ", collection_name, ' Error- ', err.message)
                return reject("collection creation failed. Name=" + collection_name + ". Error-" + err.message);
            }

            collection.insertOne(collection_schema, {}, function(err, result) {

                if (err) {
                    return reject("error creating bot_channels schema. - " + err.message)
                } else {
                    console.log("successfully created a schema for ", collection_name)

                    //TODO : create indexes
                    collection.deleteOne({}, function(error, result) {})
                    resolve("success")
                }
            })
        })
    })
}