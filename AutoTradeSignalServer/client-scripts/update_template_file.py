
import requests
import shutil

import sys,os
import argparse
# python3 update_template_file.py http://localhost:3000/updateTemplateFile  downloads/test2.tpl


parser = argparse.ArgumentParser()
parser.add_argument("url", help="url for download the file")
parser.add_argument("destination",  help="location that file need to copy with file name")
   
args = parser.parse_args()

url = str(args.url)
dest = str(args.destination)

try:
    # Make a get request to get the latest position of the international space station from the opennotify api.
    response = requests.get(url)
    #http://localhost:3000/updateTemplateFile  downloads/test1.tpl

    # Print the status code of the response.

    print(response.status_code)
    #print(response.content)

    # write response in file
    file = open("resp_text.tpl", "w")
    file.write(response.text)
    file.close()


    # Move the file in to given folder
    # response.text
    # resp_content.txt
    shutil.move("resp_text.tpl", dest)

except Exception as error:
    print (error)
       
