const express = require('express')
const multer = require('multer');
const config = require('config')

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'template_folder/')
    },
    filename: function(req, file, cb) {
        cb(null, file.originalname)
    }
})
const upload = multer({ storage: storage })

const app = express()
const port = 3000


app.get('/updateTemplateFile', function(req, res) {
    console.log("client request is recieved for downloading the template file")
    var file = __dirname + '/template_folder/' + config.fileName;
    res.download(file); // Set disposition and send it.console.log("client request is recieved for template file download")
});

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});

// It's very crucial that the file name matches the name attribute in your html
app.post('/', upload.single('template-file'), (req, res) => {
    if (req.file) {
        console.log('Uploading file...');
        var filename = req.file.filename;
        var uploadStatus = 'File Uploaded Successfully';
        res.sendFile(__dirname + '/success.html');
    } else {
        console.log('No File Uploaded');
        var filename = 'FILE NOT UPLOADED';
        var uploadStatus = 'File Upload Failed';
        res.redirect('/');
    }


});

app.post('/upload', (req, res) => {
    res.redirect('/')


});

app.listen(port, () => console.log(`Auto Trade Signal Handler listening on port ${port}!`))