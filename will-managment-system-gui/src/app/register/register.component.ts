
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, UserService, AuthenticationService } from '../_services';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
     
      private formBuilder: FormBuilder,
      private router: Router,
      private authenticationService: AuthenticationService,
      private userService: UserService,
      private alertService: AlertService
  ) {
      // redirect to home if already logged in
      if (this.authenticationService.currentUserValue) {
          this.router.navigate(['/']);
      }
  }

  ngOnInit() {
      this.registerForm = this.formBuilder.group({
          email: ['', [Validators.email, Validators.required]],
          address: [''],
          username: ['', Validators.required],
          password: ['', [Validators.required, Validators.minLength(6)]],
          image: ['']
      });
  }


  onFileChange(event) {

      const reader = new FileReader();

      if (event.target.files && event.target.files.length) {
          const [file] = event.target.files;
          reader.readAsDataURL(file);

          reader.onload = () => {

             
              this.registerForm.patchValue({
                  image: reader.result
              });

              // need to run CD since file load runs outside of zone
              //this.cd.markForCheck();
          };
      }
  }

  // convenience getter for easy access to form fields
  get f() { return this.registerForm.controls; }

  onSubmit() {
      this.submitted = true;

      // stop here if form is invalid
      if (this.registerForm.invalid) {
          return;
      }

      this.loading = true;
      this.userService.register(this.registerForm.value)
          .pipe(first())
          .subscribe(
              data => {
                  var status = data["status"];
                  if(status == "success"){
                      this.alertService.success('Registration successful', true);
                      this.router.navigate(['/login']);
                  }
                  else if (status == "error"){
                      this.alertService.error(data["data"], true);
                      this.loading = false;
                  }
              },
              error => {
                  this.alertService.error(error);        
              });
  }
}
