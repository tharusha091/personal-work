﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from '../../environments/environment';
import { User } from '../_models';


@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    public loginSuccess = false;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
        // if (localStorage.getItem('loginSuccess') == 'Yes') {
        //     this.loginSuccess = true;
        // } else {
        //     this.loginSuccess = false;

        // }
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(loginRequest: any) {
        return this.http.post<any>(`${environment.apiUrl}/login`, loginRequest)
            .pipe(map(response => {

              var status =  response.status;
              if(status == "success"){
                localStorage.setItem('currentUser', JSON.stringify(loginRequest));
                this.currentUserSubject.next(loginRequest);
                localStorage.setItem('loginSuccess', 'Yes');
                this.loginSuccess = true;
                return true;

              }
              throw ("login details are incorrect")
            }));
    }

    logout(logoutRequest) {
        return this.http.post<any>(`${environment.apiUrl}/logout`, logoutRequest)
        .pipe(map(response => {

            var status =  response.status;
            if(status == "success"){
                // remove user from local storage to log user out
                localStorage.removeItem('currentUser');
                this.currentUserSubject.next(null);
                localStorage.removeItem('loginSuccess');
                this.loginSuccess = false;
                return true;

            }
            throw ("logout not succesfull")
          }));
      
    }
}