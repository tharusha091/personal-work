import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators , ReactiveFormsModule} from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, UserService, AuthenticationService } from '../_services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  loginSuccess = false;
  returnUrl: string;
  loginUrl: string;
  showLogin = true;
  showLogout = false;
  showRegister = false;
  registerForm :  FormGroup;
  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      private authenticationService: AuthenticationService,
      private alertService: AlertService,
      private userService: UserService
  ) {   
      // redirect to home if already logged in
      if (this.authenticationService.currentUserValue) { 
          this.router.navigate([''],);
          this.loginSuccess =  this.authenticationService.loginSuccess;
      }
  }

  ngOnInit() {
      console.log("login")
      this.loginForm = this.formBuilder.group({
          username: ['', Validators.required],
          password: ['', Validators.required],
          address: ['']

    
      });

      this.registerForm = this.formBuilder.group({
        email: ['', [Validators.email, Validators.required]],
        address: [''],
        username: ['', Validators.required],
        password: ['', [Validators.required, Validators.minLength(6)]],
        image: ['']
    });

      // get return url from route parameters or default to '/'
      this.returnUrl = '/wallet';
      this.loginUrl = '/';
  }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }

  logout() {
    var obj = {
        "username": this.f.username.value ,
    };
    this.loading = false;
    this.authenticationService.logout(obj)
        .pipe(first())
        .subscribe(
            data => {
                this.loginSuccess = this.authenticationService.loginSuccess;
                this.router.navigate([this.loginUrl]);
            },
            error => {
                this.alertService.error(error);
            });
  }

  registerUser(){
      this.showRegister = true;
      this.showLogin = false;
  }

  onSubmit() {
      this.submitted = true;
      // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }
       var obj = {
          "username": this.f.username.value ,
        "password": this.f.password.value,
        "mac_address": this.f.address.value
      };
      this.loading = true;
      this.authenticationService.login(obj)
          .pipe(first())
          .subscribe(
              data => {
                    console.log(data)
                  this.loginSuccess = this.authenticationService.loginSuccess;
                  this.showLogout = true;
                  this.showLogin = false;
                  this.router.navigate([this.returnUrl]);
              },
              error => {
                  console.log("************************");
                  console.log(error)     
                  this.alertService.error(error);
                  this.loading = false;
              });
  }

  onFileChange(event) {

    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
        const [file] = event.target.files;
        reader.readAsDataURL(file);

        reader.onload = () => {

           
            this.registerForm.patchValue({
                image: reader.result
            });

            // need to run CD since file load runs outside of zone
            //this.cd.markForCheck();
        };
    }
}

// convenience getter for easy access to form fields
get fs() { return this.registerForm.controls; }

onSub() {
    this.submitted = true;
    console.log("$$$$$$$$$$$$$$$$$$$$$$")
    // stop here if form is invalid
    if (this.registerForm.invalid) {
        return;
    }

    this.loading = true;
    this.userService.register(this.registerForm.value)
        .pipe(first())
        .subscribe(
            data => {
                console.log(data);
                var status = data["status"];
                if(status == "success"){
                    this.alertService.success('Registration successful', false);
                    this.showLogin = true;
                    this.showRegister = false;
                    this.loading = false;
                }
                else if (status == "error"){
                    this.alertService.error(data["data"], false);
                    this.loading = false;
                }
            },
            error => {
                this.alertService.error(error);        
            });
}

cancle(){
    this.showLogin = true;
    this.showRegister = false;
}
}
