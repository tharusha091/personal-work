import { Component, OnInit } from '@angular/core';
import { forEach } from '@angular/router/src/utils/collection';
import { AuthenticationService } from '../_services';

@Component({
  selector: 'app-wallet',
  templateUrl: './wallet.component.html',
  styleUrls: ['./wallet.component.css']
})
export class WalletComponent implements OnInit {

  constructor(
    private authenticationService: AuthenticationService,
    ) {
        // redirect to home if already logged in
        if (this.authenticationService.currentUserValue) { 
            // this.router.navigate(['/wallet']);
            this.loginSuccess =  this.authenticationService.loginSuccess;
            console.log('this.loginSuccess 1', this.loginSuccess )
        }
        console.log('this.loginSuccess 2', this.loginSuccess )
        
    }

  currentUser = null;
  wallets = [];
  currentWallet = [];
  selectedIndex = 0;
  loginSuccess = false;

  ngOnInit() {
    console.log('this.loginSuccess 3', this.loginSuccess )

    const username = {
      "username":"savycon"
    }
    const options = {
      method: 'POST',
      body: JSON.stringify(username),
      headers: {
        'Content-Type': 'application/json'
      }
    };

    fetch('http://104.248.186.187:9000/getUserProfile', options)
    .then(response => response.json())
    .then(response => {
      this.currentUser = response;
      this.wallets =  this.populateWallets(response.data.wallets)
      // console.log(response) // Prints result from `response.json()` in getRequest
       console.log(this.wallets) // Prints result from `response.json()` in getRequest
    })
    .catch(error => console.error(error))
  }

  createWallet() {
    console.log("ssss")
    const username = {
      "username":"mrimash"
    }
    const options = {
      method: 'POST',
      body: JSON.stringify(username),
      headers: {
        'Content-Type': 'application/json'
      }
    };

    fetch('http://104.248.186.187:9000/createWallet', options)
    .then(response => response.json())
    .then(response => {
      console.log(this.currentUser)
      // this.currentUser = response;
      // this.wallets =  this.populateWallets(response.data.wallets)
      // console.log(response) // Prints result from `response.json()` in getRequest
       console.log(response) // Prints result from `response.json()` in getRequest
    })
    .catch(error => console.error(error))
  }

  createAsset() {
    console.log("Asset")
    const username = 
    {
      "username":"hkk",
      "sender_private_key":"0xbe9ae36f164d8d13ba2ab5c7229a656a6339a214e79f99664daafbdbb9d92584",
      "asset_name":"Digital-Asset-1",
      "receiver_wallet_address":"0x5FFBd50d6a5dcF3dE6bB158d27d16a2997E87694",
      "type_of_asset":"digital",
      "gas_price":200,
      "total_gas_ammount":5000000,
      "ether_ammount":1000
    }
    const options = {
      method: 'POST',
      body: JSON.stringify(username),
      headers: {
        'Content-Type': 'application/json'
      }
    };

    fetch('http://104.248.186.187:9000/createWallet', options)
    .then(response => response.json())
    .then(response => {
      console.log(this.currentUser)
      // this.currentUser = response;
      // this.wallets =  this.populateWallets(response.data.wallets)
      // console.log(response) // Prints result from `response.json()` in getRequest
       console.log(response) // Prints result from `response.json()` in getRequest
    })
    .catch(error => console.error(error))
  }

  walletSelected(event) {
    console.log('scc', this.wallets[event.target.selectedIndex])
    this.selectedIndex = event.target.selectedIndex;
    this.currentWallet = this.wallets[event.target.selectedIndex]
  }

  private populateWallets(walletList){ 
    let walletInfo = []
    walletList.forEach ( wallet =>{
      walletInfo.push(
        {
          ID: wallet._id,
          walletAddress: wallet.wallet_address,
          walletPrivateKey: wallet.wallet_private_key
        }
      )
    });
    return walletInfo;
  }
}
