export enum ErrorType {
    SystemError,
    IOError
}

export class ErrorHandler {
    constructor() {

    }

    static sendError(res: any, errorType: ErrorType, errorString: string) {
        let err:any = {}
        err.message = errorString;
        err.code = 1200

        switch (errorType) {
            case ErrorType.IOError:
                res.status(500).send(err);
                break;
            case ErrorType.SystemError:
            default:
                res.status(500).send(err);
                break;
        }
    }
}