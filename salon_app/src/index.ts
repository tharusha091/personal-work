


import { Logger } from "./common/logger";

const myArgs = process.argv.slice(2);
if (myArgs.length < 1) {
    console.log("usage: npm start <dbURL> <RestServicePort> <InstanceName>");

    console.log("  * [dbURL]        should be a valid mongoDB URL. E.g. mongodb://localhost:27017/salon");
    console.log("  * [RestServicePort]   Port to start the rest service on. If not specified, this is defaulted to 3200");
    console.log("  * [InstanceName]   Instance name. If not specified, 'default' will be used");
    process.exit(0);
}

const url = myArgs[0];
let port = 3400;
if (myArgs.length >= 2) {
    port = parseInt(myArgs[1]);

    if (myArgs.length > 2) {
        Logger.getLogger().addContext('instanceId', myArgs[2]);
    }
}
import app from "./app";

app.start(url, port);