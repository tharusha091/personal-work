import { BaseEntity } from "./base-service";
const fs = require('fs');

import * as mongoose from 'mongoose';
import { ENTITIES, COLLECTIONS, getCollectionName } from '../common_defs'

import { Logger } from '../../common/logger';

const Schema = mongoose.Schema;
import path = require('path');

class SalonServiceFactory {
    private creators = new Map;
    private handlers = new Map;

    constructor() {

        this.loadServiceHandlers(path.resolve(__dirname, '../services'))
        this.loadSchemas(path.resolve(__dirname, '../schemas'))

    }

    private loadServiceHandlers(dirPath: string) {
        Logger.getLogger().info("*****  loadServiceHandlers from path- %s *********", dirPath);
        let fileName = '';
        try {
            const dirents = fs.readdirSync(dirPath, { withFileTypes: true });
            let pattern = new RegExp(/[A-Z]*-service.js$/ig)
            dirents.forEach((dirent: any) => {
                // This check is important since, older versions of
                // files system module does not return a stat object as
                // a ouput for readdirSync (node v8.11) call.
                if (typeof (dirent) === 'string') {
                    fileName = dirent;
                    if (fs.lstatSync(path.join(dirPath, fileName)).isDirectory() || (!pattern.test(fileName))) {
                        Logger.getLogger().info("%s is ignored", fileName);
                        return
                    }

                } else {
                    fileName = dirent.name;
                    if (dirent.isDirectory() || (!pattern.test(fileName))) {
                        Logger.getLogger().info("%s is ignored", fileName);
                        return
                    }
                }

                Logger.getLogger().info("\t\t * Attempting to load handler from file - %s", fileName);
                let module = require(dirPath + '/' + fileName)

                if (module && module.createFunction && module.entityName) {
                    this.setServiceCreator(module.entityName, module.createFunction)
                }
                else
                    Logger.getLogger().info("Ignored as it is not found to be a valid")

            });

        }
        catch (err) {
            Logger.getLogger().error(" Error detecting while loading file " + fileName + ". Err: " + err);
        }
    }

    private loadSchemas(dirPath: string) {

        Logger.getLogger().info("********  loadSchemas from path- %s   *********", dirPath)

        let fileName = '';
        try {
            const dirents = fs.readdirSync(dirPath, { withFileTypes: true });

            let pattern = new RegExp(/[A-Z]*-model.js$/ig)

            dirents.forEach((dirent: any) => {
                // This check is important since, older versions of
                // files system module does not return a stat object as
                // a ouput for readdirSync (node v8.11) call.
                if (typeof (dirent) === 'string') {
                    fileName = dirent;
                    if (fs.lstatSync(path.join(dirPath, fileName)).isDirectory() || (!pattern.test(fileName))) {
                        Logger.getLogger().info("%s is ignored", fileName);
                        return
                    }

                } else {
                    fileName = dirent.name;
                    if (dirent.isDirectory() || (!pattern.test(fileName))) {
                        Logger.getLogger().info("%s is ignored", fileName);
                        return
                    }
                }

                Logger.getLogger().info("\t\t * Attempting to load schema from file - %s", fileName);
                let module = require(dirPath + '/' + fileName)

                if (module && module.Model && module.EntityName && module.EntityKey && module.SalonSchema) {
                    this.initHandlers(module.EntityName, module.EntityKey, module.Model, module.SalonSchema)
                }
                else
                    Logger.getLogger().info("Ignored as it is not found to be a valid")

            });

        }
        catch (err) {
            Logger.getLogger().error(" Error detecting while loading file " + fileName + ". Err: " + err);
        }
    }

    public getServiceHandler(entityName: string): BaseEntity | null {
        return this.handlers.get(entityName);
    }


    private initHandlers(entityName: ENTITIES, key: string, model: mongoose.Model, schema: mongoose.Schema) {
        Logger.getLogger().info("Initialising hanlder for %s with key %s", entityName, key)

        let handler = this.creators.get(entityName);
        if (handler)
            this.handlers.set(entityName, handler(entityName, key, model, schema));
        else {
            Logger.getLogger().info("creating SalonBaseEntity as no custom handler found")
            this.handlers.set(entityName, new BaseEntity(entityName, key, model, schema));
        }
    }

    private setServiceCreator(entityName: string, creationMethod) {
        Logger.getLogger().info("Handler for %s loaded", entityName);

        this.creators.set(entityName, creationMethod)
    }
}

// This is a singleton pattern. The instance varibale should be guarded within a function scope
export var ServiceFactory = (function () {
    var instance;

    function createInstance() {
        instance = new SalonServiceFactory();
        return instance;
    }

    return {
        getInstance: function () {
            if (instance == null)
                createInstance();

            return instance;
        }
    };
})();


