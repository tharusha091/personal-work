# Loader docuemntation

## To use the loader library in your nodejs application
At the following line to use package.json

"salon-loader": "git+ssh://git@bitbucket.org/yaalalabs/salon-salon"

```
import { Loader} from "salon-loader/loader";

let loader = Loader.getInstance() // this will create a singalton object

loader.init(config.loader, url);
await this.loader.connect(false);  //wait for loader to initialise

```
## methods that data can be loaded
1 loadDataFromDbDirect(dburl: string)     here data is loaded from db directly , you need to pass db url
2 loadDataFromFile(filePath: string)      
3 loadDataFromJson(fileContent: any)

