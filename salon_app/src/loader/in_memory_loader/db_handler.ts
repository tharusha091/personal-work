
import { MongoReader } from './mongo_reader';
import { Logger } from '../../common/logger';
import { DataHandler } from './data_handler';
import { COPYFILE_FICLONE } from 'constants';
import {getCollectionName} from '../common_defs'
const loki = require("lokijs");


// const sprintf = require('sprintf');
// const assert = require('assert');

// MongoBot is a direct integration of a bot to a mongodb collection and monitors a single load balanced domain.
export class DBHandler extends DataHandler   {
    
  
    private mongoReader: MongoReader;

    constructor(readonly configuration: any, readonly dburl: string){
        super(configuration);
        this.mongoReader = new MongoReader(dburl, this);
        this.dataMap = new loki(this.configuration.fileName); 
   
    }

    loadData(){
        return new Promise((resolve, reject) => {
            this.mongoReader.createConnection(this.dburl)
            .then((msg: any) => {
                let promises:any = []
            
                this.configuration.entities.forEach((entity:any)=>{
                    promises.push(
                        new Promise((resolve, reject)=>
                        {
                            let collectionName = getCollectionName(entity.entity_name);
                            if (entity.dynamic_update)
                                this.mongoReader.watchCollection(collectionName);
                              
                            this.mongoReader.loadData(collectionName )
                            .then((loadData)=>{
                                this.insertDataArray(collectionName, loadData);
                                resolve("Ok");
                            })
                            .catch((error: any) => {
                                return reject(error);
                                
                            })
                        })
                    );
                })
                Promise.all(promises)
                .then( (data: any)=>{
                    return resolve("ok");
                })
                .catch((error: any) => {
                    return reject(error);
                })
            })
            .catch((error: any) => {
                Logger.getLogger().error(' Unable to connect ', this.dburl);
                return reject(error);
            })
        })

    }

    
   
    stop() {
        if(this.mongoReader)
            this.mongoReader.stop()
        super.stop();
    }
  

}