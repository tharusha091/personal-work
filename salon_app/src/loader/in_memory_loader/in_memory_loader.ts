
import { Logger} from '../../common/logger';
import { DBHandler } from './db_handler';
import { FileHandler } from './file_handler';
import { DataHandler } from './data_handler';


export class InMemoryLoader {

    public dataHandler: any = null;

    constructor(readonly configuration: any) {

    }

    loadDataFromDB(dbURL: string = 'mongodb://localhost:27017/salon') {
        return this.loadData(new DBHandler(this.configuration, dbURL));
    }

    private loadData(dataHander: DataHandler): Promise<any>{
        if (this.dataHandler)
            Logger.getLogger().error("loader is aleady init");     //if forcefully loader is going to be init again , currently allow it. 
        
        this.dataHandler = dataHander;
        return new Promise((resolve, reject) => {
            this.dataHandler.loadData()
                .then((msg: any) => {
                    return resolve("OK");
                })
                .catch((error: any) => {
                    return reject(error);

                })
        })

    }
    onInsert(collectionName: string, document: any, ){
        this.dataHandler.onInsert(collectionName,document);
    }

    onDelete(collectionName: string, documentKey: any){
        this.dataHandler.onDelete(collectionName, documentKey)
    }
    onUpdate(collectionName: string, documentKey: any,  document: any){
        this.dataHandler.onUpdate(collectionName, documentKey,  document)
    }

    removeCollection(collectionName: string): any{
        this.dataHandler.removeCollection(collectionName);
    }


    loadFromJson(fileContent: any) {

        return this.loadData(new FileHandler(this.configuration, fileContent, ""));
    }

    loadFromFile(filePath: any) {

        return this.loadData(new FileHandler(this.configuration, null, filePath));
    }
    findValueByFilter(collectionName: string, filter: any, queryField: string){
        return this.dataHandler.findValueByFilter(collectionName, filter, queryField)
    }
    findSingleValueByFilter(collectionName: string, filter: any, queryField: string){
        return this.dataHandler.findSingleValueByFilter(collectionName, filter, queryField)
    }
    findObject(collectionName: string, filter: any){
        return this.dataHandler.findObject(collectionName, filter)
    }
    findMany(collectionName: string, filter: any) {
        return this.dataHandler.findMany(collectionName, filter);
    }
    stop() {
        if (this.dataHandler){
            this.dataHandler.stop()
            this.dataHandler = null;
        }
            
    }


}