

import { map, assignIn } from 'lodash'
import { Logger } from '../../common/logger';
import { DynamicUpdate } from '../in_memory_loader/dynamic_update';

export class DataHandler extends DynamicUpdate {

    protected dataMap: any = null;

    constructor(readonly configuration: any) {
        super();
    }

    protected dataLoad() { } //This is override by derived classes

    insertDataArray(collectionName: string, dataArray: any[]) {
        Logger.getLogger().trace("%s is loaded", collectionName);
        if (dataArray == null)
            throw (`collection ${collectionName} cannot be found `)
        const collection = this.createCollection(collectionName);
        dataArray.forEach((msg: any) => {
            collection.insert(msg);


        });

    }
    insertData(data: any, collectionName: string) {
        Logger.getLogger().trace("%s data is added to %s", JSON.stringify(data), collectionName);
        const collection = this.createCollection(collectionName);
        collection.insert(data);
    }

    removeData(documentKey: any, collectionName: string) {
        Logger.getLogger().trace("%s data is removed from %s", JSON.stringify(documentKey), collectionName);

        let collection = this.dataMap.getCollection(collectionName);
        collection.findAndRemove(JSON.stringify(documentKey))
    }

    updateData(documentKey: any, collectionName: string, update: any) {
        Logger.getLogger().trace("%s data is updated from %s. update is %s", JSON.stringify(documentKey), collectionName, update);

        let collection = this.dataMap.getCollection(collectionName);
        let existingObject = collection.findOne(JSON.stringify(documentKey));

        assignIn(existingObject, update);
        collection.update(existingObject)

    }
    ////////////////dynamic update callbacks//////////////

    onInsert(collectionName: string, document: any) {
        this.insertData(document, collectionName);
    }

    onDelete(collectionName: string, documentKey: any) {
        this.removeData(documentKey, collectionName);

    }

    onUpdate(collectionName: string, documentKey: any, update: any) {
        this.updateData(documentKey, collectionName, update);
    }
    ///////////////////////////////////////////////////

    findValueByFilter(collectionName: string, filter: any, queryField: string) {
        Logger.getLogger().trace("loader:findValueByFilter. collectionName = %s, filter = %s, queryField = %s ", collectionName, JSON.stringify(filter), queryField);

        let collection = this.dataMap.getCollection(collectionName);
        if (collection == null) {
            Logger.getLogger().error("collection is not found ", collectionName);
            return [];
        }

        let result = collection.find(filter)
        if (result)
            return map(result, queryField);
        return [];
    }

    findSingleValueByFilter(collectionName: string, filter: any, queryField: string) {
        Logger.getLogger().trace("loader:findSingleValueByFilter. collectionName = %s, filter = %s, queryField = %s", collectionName, JSON.stringify(filter), queryField);

        let collection = this.dataMap.getCollection(collectionName);
        if (collection == null) {
            Logger.getLogger().error("collection is not found ", collectionName);
            return null;
        }

        let result = collection.findOne(filter)
        if (result) {
            return result[queryField];
        }
        return null;
    }

    findObject(collectionName: string, filter: any) {
        Logger.getLogger().trace("loader:findObject. collectionName = %s, filter = %s", collectionName, JSON.stringify(filter));
        let collection = this.dataMap.getCollection(collectionName);
        if (collection == null) {
            Logger.getLogger().error("collection is not found ", collectionName);
            return null;
        }

        return collection.findOne(filter);
    }

    findMany(collectionName: string, filter: any) {
        Logger.getLogger().trace("loader:findMany. collectionName = %s, filter = %s", collectionName, JSON.stringify(filter));
        let collection = this.dataMap.getCollection(collectionName);
        if (collection == null) {
            Logger.getLogger().error("collection is not found ", collectionName);
            return null;
        }

        return collection.find(filter);
    }

    createCollection(collectionName: string): any {
        let collection = this.dataMap.getCollection(collectionName);
        if (collection)
            return collection;

        return this.dataMap.addCollection(collectionName);
    }
    removeCollection(collectionName: string): any {
        this.dataMap.removeCollection(collectionName);
    }

    stop() {
        this.dataMap = null;

    }


}