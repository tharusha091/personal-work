const Enum = require("enum");

export enum ENTITIES {
    Base = "BASE", // base entity. Not a real entitiy
    User = "USER",
    Account = "ACCOUNT",
    Order = "ORDER",
    Income = "INCOME"
   
}

export enum COLLECTIONS {
    User = "salon_users",
    Account = "salon_accounts",
    Order = "salon_orders",
    Income = "salon_incomes"
    
}

export enum FIELDS {
    

}

export const getCollectionName = function (entity: ENTITIES): string {
    switch (entity) {

        case ENTITIES.User:
            return COLLECTIONS.User;
        case ENTITIES.Account:
            return COLLECTIONS.Account;
        case ENTITIES.Order:
            return COLLECTIONS.Order;
        case ENTITIES.Income:
            return COLLECTIONS.Income;
    }
}
