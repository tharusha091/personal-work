


import { InMemoryLoader } from './in_memory_loader/in_memory_loader'
import { Logger } from '../common/logger';
import {ENTITIES, COLLECTIONS, getCollectionName } from './common_defs'

import * as mongoose from "mongoose";
import { isEmpty } from 'lodash';
import { ServiceFactory } from "./services/service_factory";

class SalonLoader {

    public inMemoryCache: InMemoryLoader;
    private serviceFactory;  
    private url:string;

    init(configuration: any, url = 'mongodb://localhost:27017/salon') {
        
        this.inMemoryCache = new InMemoryLoader(configuration);
        this.serviceFactory = ServiceFactory.getInstance();
        mongoose.Promise = global.Promise;
        
        this.url = url;

        this.setupEvents();
    }

    setupEvents() {
        const db = mongoose.connection;

        db.on('connecting', function () {
            console.log('connecting to MongoDB...');
        });

        db.on('error', function (error) {
            Logger.getLogger().debug('Error in MongoDb connection: ', error);
            mongoose.disconnect();
        });

        db.on('connected', function () {
            console.log('MongoDB connected!');
        });

        db.once('open', function () {
            console.log('MongoDB connection opened!');
        });

        db.on('reconnected', function () {
            console.log('MongoDB reconnected!');
        });

        db.on('disconnected', function () {
            Logger.getLogger().debug('MongoDb disconnected.');
        });
    }

    async connect(reconnect: boolean):Promise<any> {
        await this.loadDataFromDbDirect(this.url);
        return mongoose.connect(this.url, { autoReconnect: reconnect, useNewUrlParser: true, useCreateIndex: true, useFindAndModify:false });
    }
    async disconnect(){
        this.inMemoryCache.stop();
        await mongoose.disconnect();
    }

    getServiceHandler(entity:ENTITIES){
        return this.serviceFactory.getServiceHandler(entity) 
    }

    loadDataFromDbDirect(dburl: string) {
        return this.inMemoryCache.loadDataFromDB(dburl);
    }

    loadDataFromFile(filePath: string) {
        return this.inMemoryCache.loadFromFile(filePath);
    }
    loadDataFromJson(fileContent: any) {

        return this.inMemoryCache.loadFromJson(fileContent);
    }

    findSingleValueByFilter(entity: ENTITIES, filter: any, queryField: string) {
        return this.inMemoryCache.findSingleValueByFilter(getCollectionName(entity), filter, queryField)
        
    }
    findValueByFilter(entity: ENTITIES, filter: any, queryField: string) {
        return this.inMemoryCache.findValueByFilter(getCollectionName(entity), filter, queryField)

    }

    findOne(entity: ENTITIES, filter: any){
        return this.inMemoryCache.findObject(getCollectionName(entity), filter)
    }

    findMany(entity: ENTITIES, filter: any) {
        return this.inMemoryCache.findMany(getCollectionName(entity), filter)
    }

}

// This is a singleton pattern. The instance varibale should be guarded within a function scope
export var Loader = (function () {
    var instance;
 
    function createInstance() {
        instance = new SalonLoader();
        return instance;
    }
 
    return {
        getInstance: function () {
            if (instance == null)
                createInstance();

            return instance;
        }
    };
})();

