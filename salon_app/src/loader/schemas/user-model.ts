

import * as mongoose from 'mongoose';
import { ENTITIES, COLLECTIONS, getCollectionName } from '../common_defs'
import { validateForiegnKey } from "./schema_validators"

const Schema = mongoose.Schema;



export const SalonSchema = new Schema({
    user_id: {
        type: String,
        required: true,
        unique: true,
        maxlength: [20, 'BankID should be less than 30 characters']
    },
    name: {
        type: String,
        required: false,
        maxlength: [100, 'BankName should be less than 100 characters']
    },
    account_id: {
        type: String,
        required: false,
    },
    role: {
        type: String,
        required: false,
        enum:[
            "manager",
            "customer",
            "admin",
            "worker"
        ]
    },
    validity: {
        type: Boolean,
        required: false,
    },
    password: {
        type: String,
        required: false,
    },
    address: {
        type: String,
        required: false,
    },
    email: {
        type: String,
        required: false,
    },
    tp_number: {
        type: String,
        required: false,
    },
    image: {
        type: String,
        required: false,
    },
    status: {
        type: String,
        required: false,
        enum: [
            "active",
            "inactive"
        ]
    },
});



export const EntityName = ENTITIES.User
export const Model = mongoose.model(getCollectionName(EntityName), SalonSchema);
export const EntityKey = "user_id"
