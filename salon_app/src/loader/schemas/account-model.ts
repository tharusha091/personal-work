
import { ENTITIES, COLLECTIONS, getCollectionName } from '../common_defs'
import { validateForiegnKey } from "./schema_validators"
import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

const imageSchema = new Schema({
    encorded_image:{
        type: String,
        required: false,
    },
    text1:{
        type: String,
        required: false,
    },
    text2:{
        type: String,
        required: false,
    },
    text3:{
        type: String,
        required: false,
    },
})
const businessSchema = new Schema({
    user_activity_period:{
        type: Number,
        required: false,
    },
    promotion_enable:{
        type: Boolean,
        required: false,
    },
    customized_promotion_enable:{
        type: Boolean,
        required: false,
    },
    product_promotion:{
        type: Number,
        required: false,
    },
    service_promotion:{
        type: Number,
        required: false,
    },
    active_user_count:{
        type: Number,
        required: false,
    },
   
})


export const SalonSchema = new Schema({

    account_id: {
        type: String,
        required: true,
        unique: true,
        maxlength: [20, 'BankID should be less than 30 characters']
    },
    name: {
        type: String,
        required: true,
        maxlength: [100, 'BankName should be less than 100 characters']
    },
    template_id: {
        type: Number,
        required: false,
    },
    official_number: {
        type: String,
        required: true,
    },
    facebook_page: {
        type: String,
        required: true,
    },
    address: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: false,
    },
    images: {
        type: [imageSchema],
        required: false,
    },
    location: {
        type: String,
        required: false,
    },
    bussiness_setup: {
        type: [businessSchema],
        required: false,
    },
   
});

export const EntityName = ENTITIES.Account
export const Model = mongoose.model(getCollectionName(EntityName), SalonSchema);
export const EntityKey = "account_id"