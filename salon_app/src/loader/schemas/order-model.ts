

import * as mongoose from 'mongoose';
import { ENTITIES, COLLECTIONS, getCollectionName } from '../common_defs'
import { validateForiegnKey } from "./schema_validators"

const Schema = mongoose.Schema;



export const SalonSchema = new Schema({
    order_id: {
        type: String,
        required: true,
        unique: true,
        maxlength: [20, 'BankID should be less than 30 characters']
    },
    pair: {
        type: String,
        required: false
        
    },
    lot_size: {
        type: String,
        required: false
        
    }
});



export const EntityName = ENTITIES.Order
export const Model = mongoose.model(getCollectionName(EntityName), SalonSchema);
export const EntityKey = "order_id"
