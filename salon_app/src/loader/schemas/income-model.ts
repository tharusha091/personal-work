

import * as mongoose from 'mongoose';
import { ENTITIES, COLLECTIONS, getCollectionName } from '../common_defs'
import { validateForiegnKey } from "./schema_validators"

const Schema = mongoose.Schema;



export const SalonSchema = new Schema({
    income_id: {
        type: String,
        required: true,
        unique: true,
        maxlength: [20, 'BankID should be less than 30 characters']
    },
    pair: {
        type: String,
        required: false
        
    },
    amount: {
        type: Number,
        required: false
        
    },
    user: {
        type: String,
        required: false
        
    },
    date: {
        type: String,
        required: false
        
    }
});



export const EntityName = ENTITIES.Income
export const Model = mongoose.model(getCollectionName(EntityName), SalonSchema);
export const EntityKey = "income_id"
