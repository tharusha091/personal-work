
import { ServiceFactory } from '../../../loader/services/service_factory';

import {ENTITIES} from '../../../loader/common_defs'

const handler = ServiceFactory.getInstance().getServiceHandler(ENTITIES.User);


module.exports = {

    get: function (req, res, next) {
        handler.getAll(res)
    },
    post: function (req, res, next) {
        handler.add(req.body, res)
    }
}
module.exports.get.apiDoc = {
    "tags": [
        "Users"
    ],
    "summary": "List All Users",
    "description": "Gets a list of all `User` entities.",
    "operationId": "getUsers",
    "produces": [
        "application/json"
    ],
    "responses": {
        "200": {
            "description": "Successful response - returns an array of users",
            "schema": {
                "type": "array",
                "items": {
                    "$ref": "#/definitions/User"
                }
            }
        },
        "500": {
            "description": "Triggered due to some internal error. "
        }
    }
}

module.exports.post.apiDoc =  {
    "tags": [
        "Users"
    ],
    "summary": "Create a User",
    "description": "Creates a new instance of a `User`.",
    "operationId": "createUser",
    "parameters": [{
        "name": "body",
        "in": "body",
        "description": "A new `User` to be created.",
        "required": true,
        "schema": {
            "$ref": "#/definitions/User"
        }
    }],
    "responses": {
        "201": {
            "description": "Success. Newly created record will be sent with this response",
            "schema": {
                "$ref": "#/definitions/User"
            }
        },
        "400": {
            "description": "Validation failures",
            "schema": {
                "$ref": "#/definitions/Error"
            }
        },
        "409": {
            "description": "The server has detected another record in database with the same key. That record will be sent with this response",
            "schema": {
                "$ref": "#/definitions/User"
            }
        },
        "500": {
            "description": "Inetnal error"
        }
    }
}