
import { ServiceFactory } from '../../../../loader/services/service_factory';

import {ENTITIES} from '../../../../loader/common_defs'

const handler = ServiceFactory.getInstance().getServiceHandler(ENTITIES.Order);


module.exports = {

    get: function (req, res, next) {
        handler.getOne(req.params.orderId, res)
    },
    put: function (req, res, next) {
        handler.update(req.params.orderId, req.body, res)
    },
    delete: function (req, res, next) {
        handler.delete(req.params.orderId, res)
    }
}

module.exports.get.apiDoc = {
    "tags": [
        "Orders"
    ],
    "summary": "Get a Order",
    "description": "Gets the details of a single instance of a `Order`.",
    "operationId": "getOrder",
    "responses": {
        "200": {
            "description": "Successful response - returns a single `Order`.",
            "schema": {
                "$ref": "#/definitions/Order"
            }
        }
    },
    "parameters": [{
        "name": "orderId",
        "in": "path",
        "description": "A unique identifier for a `Order`.",
        "required": true,
        "type": "string"
    }]
}

module.exports.put.apiDoc =  {
    "tags": [
        "Orders"
    ],
    "summary": "Update a Order",
    "description": "Updates an existing `Order`.",
    "operationId": "updateOrder",
    "parameters": [{
        "name": "body",
        "in": "body",
        "description": "Updated `Order` information.",
        "required": true,
        "schema": {
            "$ref": "#/definitions/Order"
        }
    }],
    
    "responses": {
        "202": {
            "description": "Successful response."
        }
    }
}

module.exports.delete.apiDoc =  {
    "tags": [
        "Orders"
    ],
    "summary": "Delete a Order",
    "description": "Deletes an existing `Order`.",
    "operationId": "deleteOrder",
    "produces": [
        "application/json"
    ],
    "parameters": [
        {
            "name": "orderId",
            "in": "path",
            "description": "A unique identifier for a `Order`.",
            "required": true,
            "type": "string"
        }
    ],
    "responses": {
        "200": {
            "description": "Successful response. Will return the deleted entity",
            "schema": {
                "$ref": "#/definitions/Order"
            }
        },
        "400": {
            "description": "Validation failures",
            "schema": {
                "$ref": "#/definitions/Error"
            }
        },
        "404": {
            "description": "Specific Order id is not found in the system"
        },
        "500": {
            "description": "Internal error"
        }
    }
}