
import { ServiceFactory } from '../../../../loader/services/service_factory';

import {ENTITIES} from '../../../../loader/common_defs'

const handler = ServiceFactory.getInstance().getServiceHandler(ENTITIES.Account);


module.exports = {

    get: function (req, res, next) {
        handler.getOne(req.params.accountId, res)
    },
    put: function (req, res, next) {
        handler.update(req.params.accountId, req.body, res)
    },
    delete: function (req, res, next) {
        handler.delete(req.params.accountId, res)
    }
}

module.exports.get.apiDoc = {
    "tags": [
        "Accounts"
    ],
    "summary": "Get a Account",
    "description": "Gets the details of a single instance of a `Account`.",
    "operationId": "getAccount",
    "responses": {
        "200": {
            "description": "Successful response - returns a single `Account`.",
            "schema": {
                "$ref": "#/definitions/Account"
            }
        }
    },
    "parameters": [{
        "name": "accountId",
        "in": "path",
        "description": "A unique identifier for a `Account`.",
        "required": true,
        "type": "string"
    }]
}

module.exports.put.apiDoc =  {
    "tags": [
        "Accounts"
    ],
    "summary": "Update a Account",
    "description": "Updates an existing `Account`.",
    "operationId": "updateAccount",
    "parameters": [{
        "name": "body",
        "in": "body",
        "description": "Updated `Account` information.",
        "required": true,
        "schema": {
            "$ref": "#/definitions/Account"
        }
    }],
    
    "responses": {
        "202": {
            "description": "Successful response."
        }
    }
}

module.exports.delete.apiDoc =  {
    "tags": [
        "Accounts"
    ],
    "summary": "Delete a Account",
    "description": "Deletes an existing `Account`.",
    "operationId": "deleteAccount",
    "produces": [
        "application/json"
    ],
    "parameters": [
        {
            "name": "accountId",
            "in": "path",
            "description": "A unique identifier for a `Account`.",
            "required": true,
            "type": "string"
        }
    ],
    "responses": {
        "200": {
            "description": "Successful response. Will return the deleted entity",
            "schema": {
                "$ref": "#/definitions/Account"
            }
        },
        "400": {
            "description": "Validation failures",
            "schema": {
                "$ref": "#/definitions/Error"
            }
        },
        "404": {
            "description": "Specific Account id is not found in the system"
        },
        "500": {
            "description": "Internal error"
        }
    }
}