
import { ServiceFactory } from '../../../../loader/services/service_factory';

import {ENTITIES} from '../../../../loader/common_defs'

const handler = ServiceFactory.getInstance().getServiceHandler(ENTITIES.Income);


module.exports = {

    get: function (req, res, next) {
        handler.getOne(req.params.incomeId, res)
    },
    put: function (req, res, next) {
        handler.update(req.params.incomeId, req.body, res)
    },
    delete: function (req, res, next) {
        handler.delete(req.params.incomeId, res)
    }
}

module.exports.get.apiDoc = {
    "tags": [
        "Incomes"
    ],
    "summary": "Get a Income",
    "description": "Gets the details of a single instance of a `Income`.",
    "operationId": "getIncome",
    "responses": {
        "200": {
            "description": "Successful response - returns a single `Income`.",
            "schema": {
                "$ref": "#/definitions/Income"
            }
        }
    },
    "parameters": [{
        "name": "incomeId",
        "in": "path",
        "description": "A unique identifier for a `Income`.",
        "required": true,
        "type": "string"
    }]
}

module.exports.put.apiDoc =  {
    "tags": [
        "Incomes"
    ],
    "summary": "Update a Income",
    "description": "Updates an existing `Income`.",
    "operationId": "updateIncome",
    "parameters": [{
        "name": "body",
        "in": "body",
        "description": "Updated `Income` information.",
        "required": true,
        "schema": {
            "$ref": "#/definitions/Income"
        }
    }],
    
    "responses": {
        "202": {
            "description": "Successful response."
        }
    }
}

module.exports.delete.apiDoc =  {
    "tags": [
        "Incomes"
    ],
    "summary": "Delete a Income",
    "description": "Deletes an existing `Income`.",
    "operationId": "deleteIncome",
    "produces": [
        "application/json"
    ],
    "parameters": [
        {
            "name": "incomeId",
            "in": "path",
            "description": "A unique identifier for a `Income`.",
            "required": true,
            "type": "string"
        }
    ],
    "responses": {
        "200": {
            "description": "Successful response. Will return the deleted entity",
            "schema": {
                "$ref": "#/definitions/Income"
            }
        },
        "400": {
            "description": "Validation failures",
            "schema": {
                "$ref": "#/definitions/Error"
            }
        },
        "404": {
            "description": "Specific Income id is not found in the system"
        },
        "500": {
            "description": "Internal error"
        }
    }
}