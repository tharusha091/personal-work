
import { ServiceFactory } from '../../../loader/services/service_factory';

import {ENTITIES} from '../../../loader/common_defs'

const handler = ServiceFactory.getInstance().getServiceHandler(ENTITIES.Account);


module.exports = {

    get: function (req, res, next) {
        handler.getAll(res)
    },
    post: function (req, res, next) {
        handler.add(req.body, res)
    }
}

module.exports.get.apiDoc = {
    "tags": [
        "Accounts"
    ],
    "summary": "List All Accounts",
    "description": "Gets a list of all `Account` entities.",
    "operationId": "getAccounts",
    "responses": {
        "200": {
            "description": "Successful response - returns an array of `Account` entities.",
            "schema": {
                "type": "array",
                "items": {
                    "$ref": "#/definitions/Account"
                }
            }
        }
    }
}

module.exports.post.apiDoc =   {
    "tags": [
        "Accounts"
    ],
    "summary": "Create a Account",
    "description": "Creates a new instance of a `Account`.",
    "operationId": "createAccount",
    "parameters": [{
        "name": "body",
        "in": "body",
        "description": "A new `Account` to be created.",
        "required": true,
        "schema": {
            "$ref": "#/definitions/Account"
        }
    }],
    "responses": {
        "201": {
            "description": "Successful response."
        }
    }

}


