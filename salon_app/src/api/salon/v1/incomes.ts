
import { ServiceFactory } from '../../../loader/services/service_factory';

import {ENTITIES} from '../../../loader/common_defs'

const handler = ServiceFactory.getInstance().getServiceHandler(ENTITIES.Income);


module.exports = {

    get: function (req, res, next) {
        handler.getAll(res)
    },
    post: function (req, res, next) {
        handler.add(req.body, res)
    }
}

module.exports.get.apiDoc = {
    "tags": [
        "Incomes"
    ],
    "summary": "List All Incomes",
    "description": "Gets a list of all `Income` entities.",
    "operationId": "getIncomes",
    "responses": {
        "200": {
            "description": "Successful response - returns an array of `Income` entities.",
            "schema": {
                "type": "array",
                "items": {
                    "$ref": "#/definitions/Income"
                }
            }
        }
    }
}

module.exports.post.apiDoc =   {
    "tags": [
        "Incomes"
    ],
    "summary": "Create a Income",
    "description": "Creates a new instance of a `Income`.",
    "operationId": "createIncome",
    "parameters": [{
        "name": "body",
        "in": "body",
        "description": "A new `Income` to be created.",
        "required": true,
        "schema": {
            "$ref": "#/definitions/Income"
        }
    }],
    "responses": {
        "201": {
            "description": "Successful response."
        }
    }

}


