
import { ServiceFactory } from '../../../../loader/services/service_factory';
import {ENTITIES} from '../../../../loader/common_defs'

const handler = ServiceFactory.getInstance().getServiceHandler(ENTITIES.User);


module.exports = {

    get: function (req, res, next) {
        handler.getOne(req.params.userId, res)
    },
    put: function (req, res, next) {
        handler.update(req.params.userId, req.body, res)
    },
    delete: function (req, res, next) {
        handler.delete(req.params.userId, res)
    }
}
module.exports.get.apiDoc =  {
    "tags": [
        "Users"
    ],
    "summary": "Get a User",
    "description": "Gets the details of a single instance of a `User`.",
    "operationId": "getUser",
    "parameters": [{
        "name": "userId",
        "in": "path",
        "description": "A unique identifier for a `User`",
        "required": true,
        "type": "string"
    }],
    "responses": {
        "200": {
            "description": "Successful response - returns a single `User`.",
            "schema": {
                "$ref": "#/definitions/User"
            }
        },
        "404": {
            "description": "This is generated when the user is not found in the system"
        },
        "500": {
            "description": "Internal error"
        }
    }
}

module.exports.put.apiDoc =  {
    "tags": [
        "Users"
    ],
    "summary": "Update a User",
    "description": "Updates an existing `User`.",
    "operationId": "updateUser",
    "consumes": [
        "application/json"
    ],
    "produces": [
        "application/json"
    ],
    "parameters": [{
            "name": "body",
            "in": "body",
            "description": "Updated `User` information.",
            "required": true,
            "schema": {
                "$ref": "#/definitions/User"
            }
        },
        {
            "name": "userId",
            "in": "path",
            "description": "user id",
            "required": true,
            "type": "string"
        }
    ],
    "responses": {
        "202": {
            "description": "Successfull. The updated record will be sent back with this response",
            "schema": {
                "$ref": "#/definitions/User"
            }
        },
        "400": {
            "description": "Validation failure",
            "schema": {
                "$ref": "#/definitions/Error"
            }
        },
        "404": {
            "description": "The user id is not found in the system. Cannot be updated"
        },
        "409": {
            "description": "The server has detected that there is newer copy of the entity based on the ref_ver. Update is rejected.\nThe record that was found is sent with this response",
            "schema": {
                "$ref": "#/definitions/User"
            }
        },
        "500": {
            "description": "Internal error. Retry later"
        }
    }
}

module.exports.delete.apiDoc =  {
    "tags": [
        "Users"
    ],
    "summary": "Delete a User",
    "description": "Deletes an existing `User`.",
    "operationId": "deleteUser",
    "produces": [
        "application/json"
    ],
    "parameters": [{
        "name": "userId",
        "in": "path",
        "description": "A unique identifier for a `User`.",
        "required": true,
        "type": "string"
    }],
    "responses": {
        "200": {
            "description": "Succesfull. The deleted record will be sent back with this response",
            "schema": {
                "$ref": "#/definitions/User"
            }
        },
        "400": {
            "description": "Validation failures",
            "schema": {
                "$ref": "#/definitions/Error"
            }
        },
        "404": {
            "description": "User id is not found in the system"
        },
        "500": {
            "description": "Internal error"
        }
    }

}

