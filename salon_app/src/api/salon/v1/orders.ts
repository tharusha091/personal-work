
import { ServiceFactory } from '../../../loader/services/service_factory';

import {ENTITIES} from '../../../loader/common_defs'

const handler = ServiceFactory.getInstance().getServiceHandler(ENTITIES.Order);


module.exports = {

    get: function (req, res, next) {
        handler.getAll(res)
    },
    post: function (req, res, next) {
        handler.add(req.body, res)
    }
}

module.exports.get.apiDoc = {
    "tags": [
        "Orders"
    ],
    "summary": "List All Orders",
    "description": "Gets a list of all `Order` entities.",
    "operationId": "getOrders",
    "responses": {
        "200": {
            "description": "Successful response - returns an array of `Order` entities.",
            "schema": {
                "type": "array",
                "items": {
                    "$ref": "#/definitions/Order"
                }
            }
        }
    }
}

module.exports.post.apiDoc =   {
    "tags": [
        "Orders"
    ],
    "summary": "Create a Order",
    "description": "Creates a new instance of a `Order`.",
    "operationId": "createOrder",
    "parameters": [{
        "name": "body",
        "in": "body",
        "description": "A new `Order` to be created.",
        "required": true,
        "schema": {
            "$ref": "#/definitions/Order"
        }
    }],
    "responses": {
        "201": {
            "description": "Successful response."
        }
    }

}


