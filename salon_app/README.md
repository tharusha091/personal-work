```
usage: npm start <dbURL> <RestServicePort> <InstanceName>
  * [dbURL]        should be a valid mongoDB URL. E.g. mongodb://localhost:27017/salon
  * [RestServicePort]   Port to start the rest service on. If not specified, this is defaulted to 3200
  * [InstanceName]   Instance name. If not specified, 'default' will be used
```
This will create a refdata REST API server at the given port.

API documentation can be found at <url>:port/docs. E.g. http://localhost:3200/docs