var MongoClient = require('mongodb').MongoClient,
    test = require('assert');

var mongourl = 'mongodb://localhost:27017';


// node pre_run.js 'mongodb://localhost:27017' 0

const args = process.argv.slice(2);
if (args.length == 1) {
    mongourl = args[0];
} else {
    console.log('Database url is not specified. Defaulting.')
}
var action = 0;
if (args.length == 2) {

    action = args[1];
}



console.log('Using %s', mongourl)

MongoClient.connect(mongourl, { useNewUrlParser: true }, function(err, client) {
    if (err) {
        console.log(" Error connecting to DB URL. Details: " + err);
        process.exit();
    }
    var db = client.db('salon');


    var promises1 = [
        //------------ for cleaning up old tables ---------


        db.dropCollection('salon_accounts'),
        db.dropCollection('salon_users'),
        db.dropCollection('salon_services'),
        db.dropCollection('salon_products'),
        db.dropCollection('salon_reviews'),
        db.dropCollection('salon_messages'),
        db.dropCollection('salon_posts'),
        db.dropCollection('salon_bills'),
        db.dropCollection('salon_orders'),
        db.dropCollection('salon_incomes')

    ]

    Promise.all(promises1)
        .then((smg) => {
            console.log(smg)
        })
        .catch((err) => {
            // console.log(err);
        })
        .finally(() => {
            console.log("all collections dropped")

            var promises2 = [



                creatCollection(db, 'salon_accounts'),
                creatCollection(db, 'salon_users'),
                creatCollection(db, 'salon_services'),
                creatCollection(db, 'salon_products'),
                creatCollection(db, 'salon_reviews'),
                creatCollection(db, 'salon_messages'),
                creatCollection(db, 'salon_posts'),
                creatCollection(db, 'salon_bills'),
                creatCollection(db, 'salon_orders'),
                creatCollection(db, 'salon_incomes')


            ]

            Promise.all(promises2)
                .then((smg) => {
                    console.log(smg)
                })
                .catch((err) => {
                    console.log(err);
                })
                .finally(() => {
                    // ['./salon_accounts.json', './salon_users.json', './salon_services.json', './salon_products.json'].forEach((path) => {
                    //     loadData(db, path);
                    // })

                    client.close();
                })
        })
})


async function loadData(db, path) {
    var records = require(path);
    for (let key in records) {
        console.log("Found records for ", key)

        try {
            let collection = await getCollection(db, key)

            if (action == 1)
                await collection.deleteMany({})

            await collection.insertMany(records[key])
            console.log(" %d records successfully inserted", records[key].length)
        } catch (error) {
            console.error("Error: ", error)

        }
    }
}

getCollection = function(db, name) {
    // console.info("accessing collection .......", name);
    return new Promise((resolve, reject) => {
        db.collection(name, { strict: true }, (err, collection) => {
            if (err) {
                return reject('Error: ' + err);
            }
            resolve(collection);
        });
    });
}


var creatCollection = function(db, collection_name, collection_schema = {}, options = {}) {
    return new Promise(function(resolve, reject) {
        //console.log("creating collection ", collection_name, " with schema ", JSON.stringify(collection_schema))
        db.createCollection(collection_name, options, function(err, collection) {

            if (err) {
                console.log("Could not create the collection ", collection_name, ' Error- ', err.message)
                return reject("collection creation failed. Name=" + collection_name + ". Error-" + err.message);
            }

            collection.insertOne(collection_schema, {}, function(err, result) {

                if (err) {
                    return reject("error creating salon_channels schema. - " + err.message)
                } else {
                    console.log("successfully created a schema for ", collection_name)

                    //TODO : create indexes
                    collection.deleteOne({}, function(error, result) {})
                    resolve("success")
                }
            })
        })
    })
}