from __future__ import unicode_literals

from django.db import models

import django_tables2 as tables

class RealtimeResultTable(tables.Table):
    class Meta:
        template_name = 'django_tables2/bootstrap.html'
    response_id = tables.Column(accessor='id')
    # source_id = tables.Column(accessor='sourceid')
    # plugin_id = tables.Column(accessor='pluginId')
    # cwe_id = tables.Column(accessor='cweid')
    # wasc_id = tables.Column(accessor='wascid')
    # message_id = tables.Column(accessor='messageId')
    # url = tables.Column(accessor='url')  
    method = tables.Column(accessor='method')
    # description = tables.Column(accessor='description')
    evidence = tables.Column(accessor='evidence')
    confidence = tables.Column(accessor='confidence')
    # alert = tables.Column(accessor='attack')
    name = tables.Column(accessor='name')
    attack = tables.Column(accessor='risk')
    reference = tables.Column(accessor='reference')

class ResponseTable(tables.Table):
    class Meta:
        template_name = 'django_tables2/bootstrap.html'
    method = tables.Column(accessor='method')

class RequestTable(tables.Table):
    class Meta:
        template_name = 'django_tables2/bootstrap.html'
    method = tables.Column(accessor='method')

# Create your models here.
