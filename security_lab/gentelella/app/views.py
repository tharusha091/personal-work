from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse
from django_datatables_view.base_datatable_view import BaseDatatableView
from django.utils.html import escape
from zapv2 import ZAPv2
import time
from django.http import JsonResponse
from .models import RealtimeResultTable
from .models import ResponseTable
from .models import RequestTable
import json
from django_tables2 import RequestConfig
from django.conf import settings



def index(request):
    data = []
    if request.method == 'GET':

        if "attackType" in request.GET:
            testUrl = request.GET.get('url')
            attackType = request.GET.get("attackType")
            if "Attack" in request.GET:
                response = executeScript(attackType, testUrl)
                data = list(response)
                print(list(response))
    

   
    reailtime_result = RealtimeResultTable(data)
    request_table = RequestTable(data)
    response_table = ResponseTable(data)
    # RequestConfig(request).configure(reailtime_result) 
    context = {'reailtime_result' : reailtime_result, 'response_table': response_table, 'request_table' : request_table}

    return render(request, 'app/index.html' ,context)

def realtime_results(request):
    context = {}
    template = loader.get_template('app/realtime_results.html')
    return HttpResponse(template.render(context, request))
    
def gentella_html(request):
    context = {}
    # The template to be loaded as per gentelella.
    # All resource paths for gentelella end in .html.

    # Pick out the html file name from the url. And load that template.
    load_template = request.path.split('/')[-1]
    template = loader.get_template('app/' + load_template)
    return HttpResponse(template.render(context, request))

def executeScript(attackType, url):
    print(attackType + " : " + url)

    apikey = settings.API_KEY

    try:
        # By default ZAP API client will connect to port 8080
        zap = ZAPv2(apikey=apikey)
        # Use the line below if ZAP is not listening on port 8080, for example, if listening on port 8090
        # zap = ZAPv2(apikey=apikey, proxies={'http': 'http://localhost:8080/?anonym=true&app=ZAP', 'https': 'http://localhost:8080/?anonym=true&app=ZAP'})

        #target = getValidTarget(url)
        target = url
        #target = 'http://localhost:4200'
        # do stuff
        print ('Accessing target %s' % target)
        # try have a unique enough session...



        zap.urlopen(target)
        # Give the sites tree a chance to get updated
        print("REQ ", zap._request)
        executetask(zap, target, attackType)
        
        print ('Scanning target %s' % target)
        scanid = zap.ascan.scan(target)
        while (int(zap.ascan.status(scanid)) < 100):
            print ('Scan progress %: ' + zap.ascan.status(scanid))
            time.sleep(5)

        print ('Scan completed')

        # Report the results

        print ('Hosts: ' + ', '.join(zap.core.hosts))
        print ('Alerts: ')
       
        return zap.core.alerts()

    except Exception as error:
        print (error)
        return str(error)

def executetask(zap, target, attackType):
    time.sleep(2)

    print(attackType)
    if attackType == "Spider":
        
        scanid = zap.spider.scan(target)
            # Give the Spider a chance to start
        time.sleep(2)
        while (int(zap.spider.status(scanid)) < 100):
            print ('Spider progress %: ' + zap.spider.status(scanid))
            time.sleep(2)

    elif attackType == "AJAX Spider":
        
        scanid = zap.ajaxSpider.scan(target)
            # Give the Spider a chance to start
        time.sleep(2)
        while (int(zap.ajaxSpider.status(scanid)) < 100):
            print ('AJAX Spider progress %: ' + zap.ajaxSpider.status(scanid))
            time.sleep(2)

    elif attackType == "Scanner":
    
        scanid = zap.ascan.scan(target)
            # Give the Spider a chance to start
        time.sleep(2)
        while (int(zap.ascan.status(scanid)) < 100):
            print ('Scanner progress %: ' + zap.ascan.status(scanid))
            time.sleep(2)

    elif attackType == "Fuzzer":
        print("not implemeated")
        #  scanid = zap.ajaxSpider.scan(target)
        #     # Give the Spider a chance to start
        # time.sleep(2)
        # while (int(zap.ajaxSpider.status(scanid)) < 100):
        #     print ('Spider progress %: ' + zap.ajaxSpider.status(scanid))
        #     time.sleep(2)

    elif attackType == "Port Scan":
    
        scanid = zap.pscan.scan(target)
            # Give the Spider a chance to start
        time.sleep(2)
        while (int(zap.pscan.status(scanid)) < 100):
            print ('Port Scan progress %: ' + zap.pscan.status(scanid))
            time.sleep(2) 
    print ('task is  completed')
       
    time.sleep(5)

# def addToList():
#     data.append({'name': 'rimash'})