// Copyright 2018 Yaala Labs (www.yaalalabs.com). All rights reserved.

import { DATABASE_EVENTS, DB_COLLECTIONS, getCollectionName } from './common_defs';
import { Logger } from './logger';
import { CommunicatorListner, Communicator } from './communicator_interface'
import { InOrderBulkQueue } from './mongo_helpers'

const MongoClient = require('mongodb').MongoClient;



// MongoBot is a direct integration of a bot to a mongodb collection and monitors a single load balanced domain.
export class MongoCommunicator implements Communicator {
    private collectionQueues: InOrderBulkQueue[] = []; 
    private messageQueue: InOrderBulkQueue | undefined = undefined;
    private messageQueueName: string;
    private client: any = null;
    private cursorQ: any = null;
    private db: any = null;
    private connectionStaus: boolean = false

    dbEventCallback: any = function (dbEvent: DATABASE_EVENTS) {
        Logger.getLogger().info("DB event not handled - %s", dbEvent.toString());
    };

    distributer: any = new Map()

    stopping = false;
    reconnect = false;

    constructor(readonly ID: string, readonly collectionNames: DB_COLLECTIONS[], readonly messageQueueId : DB_COLLECTIONS, readonly silent: boolean = true) {
        this.messageQueueName = getCollectionName(this.messageQueueId);
        Logger.getLogger().addContext("instanceId", ID);
        if (!this.silent) {
            Logger.getLogger().info("Initlaising comms for ", this.ID)
            Logger.getLogger().info("\t MessageQueue -------> ", this.messageQueueName);
            Logger.getLogger().info("\t collection names -> ", this.collectionNames);
           
        }

    }



    setDBEventCallback(dbEventCallback: any) {
        this.dbEventCallback = dbEventCallback
    }
    subscribe(topic: string, listener: CommunicatorListner) {
        let subs = this.distributer.get(topic);
        if (subs == undefined) {
            subs = [];
            subs.push(listener)
            this.distributer.set(topic, subs)
        }
        else if (subs.indexOf(listener) === -1) { //avoid having the same listner again
            subs.push(listener)
        }
    }
    unsubscribe(topic: string, listener: CommunicatorListner) {
        let subs = this.distributer.get(topic);
        if (subs !== undefined) {
            let index = subs.indexOf(listener);

            subs.splice(index, 1);
            if (subs.length === 0)
                this.distributer.delete(topic);
        }
    }
    send(msg: any) {
        //console.log("send ------------> ", msg)
        if (this.messageQueue)
            this.messageQueue.insertTx(msg)
    }

    processMsg(doc: any) {
        // console.log("processMsg ",JSON.stringify(doc))
        if (Array.isArray(doc.to)) {
            doc.to.forEach((key: string) => {
                let listeners = this.distributer.get(key)
                if (listeners) {
                    listeners.forEach((listener: CommunicatorListner) => {
                        listener.onInputMsg(doc)
                    })
                }
            })
        } else { // GUI is not sending it in an array as standard specify. will be remved later once GUI fix
            let listeners = this.distributer.get(doc.to)

            if (listeners) {
                listeners.forEach((listener: CommunicatorListner) => {
                    listener.onInputMsg(doc)
                })
            }
        }
    }

    start(dbURL = 'mongodb://localhost:27017/lime', reconnect = false) {
        if (!this.silent)
            Logger.getLogger().info("logging into DB=%s", dbURL);

        return new Promise((resolve, reject) => {
            MongoClient.connect(dbURL, { useNewUrlParser: true, ignoreUndefined: true }, 
            async (err: any, client: any) => {
                if (err) {
                    Logger.getLogger().error("%s Error connecting to DB URL. Details: %s", this.ID, err);
                    return reject(err);
                }

                try {
                    this.db = client.db();
                    this.reconnect = reconnect;
                    this.db.on('close', () => {
                        if (!this.silent)
                            Logger.getLogger().info('%s Disconnected from database', this.ID);
                        this.connectionStaus = false
                    });
                    this.db.on('reconnect', () => {
                        Logger.getLogger().info('%s Reconnected to database', this.ID);
                        this.connectionStaus = true
                        this.dbEventCallback(DATABASE_EVENTS.reconnect);
                    });

                    this.connectionStaus = true
                    this.client = client;
                    if(this.messageQueueName !== ''){
                        let messageQueueCollection = await this.getCollection(this.db, this.messageQueueName);
                        this.messageQueue = new InOrderBulkQueue(this.messageQueueName, messageQueueCollection, 2000);
                        this.collectionQueues[this.messageQueueId] = this.messageQueue;
                    }

                    for(var collection  of this.collectionNames){
                        let collectionName = getCollectionName(collection);
                        let dbCollection = await this.getCollection(this.db, collectionName);

                        this.collectionQueues[collection] = new InOrderBulkQueue(collectionName, dbCollection, 2000); 
                    }

                    if (!this.silent)
                        Logger.getLogger().info("\n %s Initialisation done. calling listen", this.ID);
                    if(this.messageQueueName !== ''){
                        this.listen();
                
                    }
                }    
                catch (err) {
                    Logger.getLogger().error(err);
                    return reject(err);
                }

                return resolve("ok");

            });
        });
    }

    listen(attempt = 0) {
        if (!this.silent)
            Logger.getLogger().info("%s Listening on %s", this.ID, this.messageQueueName);

        this.cursorQ = this.messageQueue!.getCollection().find({ 'ts': { $gte: new Date() } }, { tailable: true, awaitData: true });
        this.cursorQ.forEach((doc: any) => {
            if (attempt > 0) {
                Logger.getLogger().info('%s  resumed listening after reconnection', this.ID);

                // Not all errors are disconnections, hence on('reconnect') event might not get fired. We need to fire by tracking the DB status
                if ( this.connectionStaus == false)
                    this.dbEventCallback(DATABASE_EVENTS.reconnect);

                this.connectionStaus = true;
                attempt = 0;
            }
            this.processMsg(doc);

        }, (error: any) => {
            this.cursorQ = null;
            if (!this.stopping) {
                if (error) {
                    Logger.getLogger().error('%s Error while listening to message queue: ', this.ID, error.message);
                } else {
                    Logger.getLogger().error('%s Error while listening to message queue', this.ID);
                }
                // Not all errors are disconnections, hence on('reconnect') event might not get fired. We need to fire by tracking the DB status
                this.connectionStaus = false

                if (attempt === 0) {
                    Logger.getLogger().error('%s Disconnected triggered due to error when listening to msg queue', this.ID);
                    this.dbEventCallback(DATABASE_EVENTS.disconnect);
                }

                if (this.reconnect) {
                    attempt++;
                    Logger.getLogger().info('%s Attempt %d to resume message queue in 5 seconds', this.ID, attempt);
                    setTimeout(() => {
                        this.listen(attempt);
                    }, 5000);
                }
            }
        });
    }

    async stop() {
        Logger.getLogger().trace("%s MongoCommunicator::stop()", this.ID);
        this.stopping = true;
        if (this.cursorQ) {
            this.cursorQ.close();
        }

        this.collectionQueues.forEach((queue: InOrderBulkQueue)=>{
            if(queue)
                queue.stop();
        })

        if (this.client) {
            this.client.close();
            this.client = null;
        }
        this.connectionStaus = false
    }

    getCollection(db: any, name: string) {
        if (!this.silent)
            Logger.getLogger().info("Checking collection .......", name);
        return new Promise((resolve, reject) => {
            db.collection(name, { strict: true }, (err: any, collection: any) => {
                if (err) {
                    return reject(`collection not found [${name}] error :${err}`);
                }
                if (!this.silent)
                    Logger.getLogger().info("           OK", name);
                resolve(collection);
            });
        });
    }




    update(collection: DB_COLLECTIONS, doc: any, filter: any){
        let collectionQueue = this.collectionQueues[collection];
        if(collectionQueue)
            collectionQueue.updateTx(filter, doc);
    }
    insert(collection: DB_COLLECTIONS, doc: any){
        let collectionQueue = this.collectionQueues[collection];
        if(collectionQueue)
            collectionQueue.insertTx(doc);
    }
    upsert(collection: DB_COLLECTIONS, doc: any, filter: any, update: boolean){
        let collectionQueue = this.collectionQueues[collection];
        if(collectionQueue){
            if (update === true) {
                collectionQueue.updateTx(filter, doc);
            }
            else {

                collectionQueue.insertTx(doc)
            }
        }
        
    }


    find(collection : DB_COLLECTIONS,filter: any, projection: any = {}, toArray: boolean = false, sort: any = {}): Promise<any>  {
        let collectionQueue = this.collectionQueues[collection];
        if(toArray === false){
            return collectionQueue.getCollection().find(filter, projection);
        }
        return collectionQueue.getCollection().find(filter, projection).sort(sort).toArray();
    }

    deleteMany(collection : DB_COLLECTIONS, filter: any){
        let collectionQueue = this.collectionQueues[collection];
        return collectionQueue.deleteTx({ deleteMany :  { filter : filter } });
    } 
    deleteOne(collection : DB_COLLECTIONS, filter: any){
        let collectionQueue = this.collectionQueues[collection];
        return collectionQueue.deleteTx({ deleteOne :  { filter : filter } });
       
    } 

    findOne(collection : DB_COLLECTIONS, filter: any): Promise<any>{
        //return this.collectionQueues[collection].getCollection().findOne(filter);
        let collectionQueue = this.collectionQueues[collection];
        return collectionQueue && collectionQueue.getCollection().findOne(filter);
    }

   

   
}
