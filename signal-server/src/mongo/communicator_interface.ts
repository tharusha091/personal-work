

export interface CommunicatorListner{
    onInputMsg(msg:any):void;

}

export interface Communicator{
    subscribe(topic:string, listener:CommunicatorListner):void
    unsubscribe(topic:string, listener:CommunicatorListner):void
    send(msg:any):void; 
}