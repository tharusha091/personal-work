




export enum DB_COLLECTIONS {
    client_details = 0,
    message_queue,
    none
   
}


export enum DATABASE_EVENTS {
    disconnect = 0,
    reconnect
}



export function getCollectionName(collection: DB_COLLECTIONS): string{

    switch(collection){
        case DB_COLLECTIONS.client_details:
            return "client_details";
       
        default:
            return '';
                 
    }
}
export function getCollectionID(collectionName: string): DB_COLLECTIONS{

    switch(collectionName){
        case "client_details":
            return DB_COLLECTIONS.client_details;
      
        default:
            return DB_COLLECTIONS.none;

    }
}




