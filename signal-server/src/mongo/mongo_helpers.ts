import { Logger } from './logger';
// This class will handle in order insertions/updates (i.e. MongoDB will by NOT gurantee ordinality across multiple write operations)
// It will also handle DB failover and associated re-tries

export class InOrderBulkQueue{
    private txSeq: number=0
    private waitingForCompletion = false;
    private bulkArray: any = [];
    private timer:any = null;
    private stopped:boolean =false

    constructor(readonly queueName:string,readonly collection:any, readonly timeoutMillisSeconds:number){
    }

    stop(){
        this.stopped = true;
        this.clearTimer();
    }

    clearTimer(){
        if (this.timer){
            clearTimeout(this.timer)
            this.timer=null
        }
    }
    updateTx(filter:any, doc: any){
        let wrapper:any ={}
        wrapper.updateOne={}
        wrapper.updateOne.update = { $set: doc}
        wrapper.updateOne.filter = filter
        wrapper.updateOne.upsert = true

        wrapper.txSeq    = this.txSeq++;
        this.bulkArray.push(wrapper)

        if (this.waitingForCompletion){
            Logger.getLogger().debug("[InOrderQueue][%s] [%d] queued", this.queueName, wrapper.txSeq );
            return;
        }

        this.doBulk()
    }

    getCollection(){
        return this.collection;
    }

    insertTx(doc: any)
    {
        let wrapper:any ={}
        wrapper.insertOne={}
        wrapper.insertOne.document = doc

        wrapper.txSeq    = this.txSeq++;

        this.bulkArray.push(wrapper)

        if (this.waitingForCompletion){
            Logger.getLogger().debug("[InOrderQueue][%s] [%d] queued", this.queueName, wrapper.txSeq );
            return;
        }

        this.doBulk()
    }
    deleteTx(filterWrapper){
        
        let wrapper:any =filterWrapper;
        wrapper.txSeq    = this.txSeq++;

        this.bulkArray.push(wrapper)

        if (this.waitingForCompletion){
            Logger.getLogger().debug("[InOrderQueue][%s] [%d] queued", this.queueName, wrapper.txSeq );
            return;
        }

        this.doBulk()
    }

    doBulk() {
        Logger.getLogger().trace("[InOrderQueue][%s] doBulk() Qlength %s", this.queueName, this.bulkArray.length);
        if ((this.bulkArray.length <= 0 ) || (this.stopped)){
            this.waitingForCompletion = false;
    
            this.clearTimer()
            return;  
        }
                 
        let arr = this.bulkArray;
        this.bulkArray = [];

        const start  =  arr[0].txSeq ;
        const end    =  arr[arr.length - 1].txSeq ;

        //update sent_time for inserts.  needed by GUIs. We will implement a generalised instrumentation later
        let sentTime = Date.now();
        arr.forEach( (wrapper:any)=>{
            if(wrapper.insertOne !== undefined){
                wrapper.insertOne.document["ts_sent"] = sentTime;
            }
        })
        this.waitingForCompletion = true;
        this.collection.bulkWrite(arr)
        .then((result: any) => {
            Logger.getLogger().debug("[InOrderQueue][%s] SendBulk- [%d] to [%d] confirmed", this.queueName, start, end)      
            this.doBulk()
        })
        .catch((error: any) => {    
                 
            // Note: by deafualt bulkWrite will do an 'ordered' operation. Hence if 'm' documents were inserted and 'n' successfull, 
            //       n+1 to has failed. They need to be retried, IFF the error is due a DB failure.
            //       while the bulk operation is happening (async), there could be more inserts to the bulkArray. Hence we need to recreate the
            //       bulkArray with failed txs insert to the front
           
            let successCount = 0;
            successCount = error.nInserted + error.nUpserted
            
            Logger.getLogger().debug("[InOrderQueue][%s] error in bulk insertion error - [%s] , success count - [%s]", this.queueName, error, successCount);

            if ((isNaN(successCount) === false ) && (successCount < arr.length)){
                Logger.getLogger().error("[InOrderQueue][%s] SendBulk- [%d] to [%d] failed", this.queueName, (start + successCount), end);

                arr = arr.concat(this.bulkArray)
                arr.concat(this.bulkArray)  
                this.bulkArray = arr.slice(successCount) ;  
                if (! this.timer){
                    Logger.getLogger().debug("[InOrderQueue][%s] retry timer started", this.queueName) 
                    this.timer = setInterval(() => {
                        this.doBulk();
                    }, this.timeoutMillisSeconds);
                }
            }
            else{
                if(arr.length > 0){
                    let promises:any = [];
                    arr.forEach((doc:any)=>{
    
                        promises.push(
                            new Promise((resolve, reject)=>
                            {
                                if((doc.insertOne !== undefined)){
                                    this.collection.insertOne( doc.insertOne.document)
                                    .then((result: any) => {
                                        Logger.getLogger().debug("[InOrderQueue][%s] insertOne confirmed", this.queueName);
                                        resolve("ok");
                                    })
                                    .catch((error:any) => {
                                        Logger.getLogger().error("[InOrderQueue][%s] insertOne failed, error = %s", this.queueName, error);
                                        resolve("ok");
                                    });

                                }
                                else if(doc.updateOne !== undefined){
                                    this.collection.updateOne( doc.updateOne.filter, doc.updateOne.update, {upsert: doc.updateOne.upsert})
                                    .then((result: any) => {
                                        Logger.getLogger().debug("[InOrderQueue][%s] updateOne confirmed", this.queueName);
                                        resolve("ok");
                                    })
                                    .catch((error:any) => {
                                        Logger.getLogger().error("[InOrderQueue][%s] updateOne failed, error = %s", this.queueName, error);
                                        resolve("ok");
                                    });

                                }
                            })
                        );
                    })
                    Promise.all(promises)
                    .then( (data: any)=>{
                        this.doBulk();
                    })
                    .catch((error: any) => {
                        
                    })
                }
            }
        })
    }
}
