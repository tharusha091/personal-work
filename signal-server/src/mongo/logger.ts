// Copyright 2018 Yaala Labs (www.yaalalabs.com). All rights reserved.

export enum LOG_LEVEL {
    level_1 = 1,
    level_2,
    level_3,
    level_4,
    level_5
}

// This is a singleton pattern. The instance varibale should be guarded within a function scope
export var Logger = (function () {
    let logLevel = null;
    let LOG = null;

    function createLogger() {
        
        const log4js = require('log4js');

        const config = require('config');
        log4js.configure(config.get('log4js'));
        logLevel = config.log4js.log_level;
        LOG = log4js.getLogger('app');
        LOG.originalDebugMethod = LOG.debug;

        LOG.debug = (message: string, ...args: any[]) => {
            debugLevel(LOG_LEVEL.level_2, message, ...args)
        }

        LOG.trace = (message: string, ...args: any[]) => {
            debugLevel(LOG_LEVEL.level_1, message, ...args)
        }

        LOG.debugWithLevel = (level: LOG_LEVEL, message: string, ...args: any[]) => {
            debugLevel(level, message, ...args)
        }

        function debugLevel(level: LOG_LEVEL, message: string, ...args: any[]) {
            if (level >= logLevel)
                LOG.originalDebugMethod(message, ...args);
        }
        return LOG;
    }

    return {
        getLogger: function () {
            if (LOG == null)
                createLogger();

            return LOG;
        }
    };
})();
