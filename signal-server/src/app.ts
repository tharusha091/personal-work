// Copyright 2018 Yaala Labs (www.yaalalabs.com). All rights reserved.

import * as express from "express";
import * as bodyParser from "body-parser";
import { Routes } from "./rest/routes";

import { Utils } from "./system/signal-utils";
const config = require('config');

export class App {

    app: express.Application;
    routePrv: Routes = new Routes();
   
    instanceId: string;
    
    private dbUrl = '';
    private port = 0;

    constructor() {
      
        this.app = express();
        this.configure();
        
    }

    init(url: string, port: number, instanceId: string): Promise<any> {
        this.dbUrl = url;
        this.port = port;
        this.instanceId = instanceId;
        
        return new Promise((resolve, reject) => {

            Utils.initDataHander(this.instanceId, this.dbUrl)
            .then(() => {
                resolve("ok")
            })
            .catch((err) => {
                reject(err);
            });


        });
    }

    start() {
        this.app.listen(this.port, () => {
            console.log('Express server listening on port ' + this.port);
        })
    }

    private configure(): void {
        // support application/json type post data
        this.app.use(bodyParser.json());
        // support application/x-www-form-urlencoded post data
        this.app.use(bodyParser.urlencoded({ extended: false }));
        // configure
        this.routePrv.configureRoutes(this.app);
    }

}

export default new App();