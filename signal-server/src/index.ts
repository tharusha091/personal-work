// Copyright 2018 Yaala Labs (www.yaalalabs.com). All rights reserved.

import app from "./app";
import { LOG } from "./utils/logger";

const args = process.argv.slice(2);
if (args.length === 1 && args[0] === '--help') {
    console.log("usage: npm start <RestServicePort> <InstanceName>");

    console.log("  * [RestServicePort]   Port to start the rest service on. If not specified, this is defaulted to 3400");
    console.log("  * [InstanceName]   Buttler instance name. If not specified, 'default' will be used");
    process.exit(0);
}

const url = "mongodb://localhost:27017/signal";
let port = 3600;
let instanceId = 'signal_01';

if (args.length >= 1) {
    port = parseInt(args[0]);

    if (args.length > 1) {
        instanceId = args[1];
        LOG.addContext('instanceId', args[1]);
    }
}

app.init(url, port, instanceId).then(() => {
    app.start();
}).catch((err) => {
    LOG.error('Failed to start signal module. Error: ' + err);
    process.exit(-1);
})