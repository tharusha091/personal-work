import { signalDataHandler } from "./signal-data-handler";
import { Utils } from "./signal-utils";
import { DB_COLLECTIONS } from "../mongo/common_defs";

export class Client {
    lastSeq: number;
    supportReal: boolean;
    accountID: string;
    accountName: string;
    accountType: string;
    connected: boolean;

    constructor(clientObject: any){
        this.lastSeq = clientObject.lastSeq;
        this.supportReal = clientObject.supportReal;
        this.accountID = clientObject.accountID;
        this.accountName = clientObject.accountName;
        this.accountType = clientObject.accountType;
        this.connected = false;
        this.saveClient();
    }

    saveClient(){
        let clientObject = {lastSeq: this.lastSeq, supportReal: this.supportReal, accountID: this.accountID, accountName: this.accountName};
        Utils.getDataHander().getMongoCommunicator().insert(DB_COLLECTIONS.client_details, clientObject );
    }

    setLastSeq(seq: number, saveInDB = true){
        this.lastSeq = seq;
        Utils.getDataHander().getMongoCommunicator().update(DB_COLLECTIONS.client_details, {lastSeq: this.lastSeq}, {accountID: this.accountID});
    }
    onConnect(saveInDB = true){
        this.connected = true;
        Utils.getDataHander().getMongoCommunicator().update(DB_COLLECTIONS.client_details, {connected: this.connected}, {accountID: this.accountID});
    }
    onDisConnect(saveInDB = true){
        this.connected = false;
        Utils.getDataHander().getMongoCommunicator().update(DB_COLLECTIONS.client_details, {connected: this.connected}, {accountID: this.accountID});
    }

    getConnectionStatus(){
        return this.connected;
    }

   
}