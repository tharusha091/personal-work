
import { LOG } from '../utils/logger';
import { ErrorHandler } from '../utils/error-handler';
import {MongoCommunicator} from '../mongo/mongo_communicator';
import { DB_COLLECTIONS} from '../mongo/common_defs';
import { Client } from './client';


export interface signalDataEventCallback  {

}

export class signalDataHandler {
    mongoCommunicator: MongoCommunicator;
  
    clientDetails: any;
    messageCache: any;
    dataCallback: signalDataEventCallback[] =[];
    currentSeq: number;
    connectedClientCount: number;

    constructor(instanceId: string) {
        this.mongoCommunicator = new MongoCommunicator(instanceId, [DB_COLLECTIONS.client_details], DB_COLLECTIONS.none, false);
        this.clientDetails = new Map();
        this.messageCache = new Map();
        this.connectedClientCount = 0;
        this.currentSeq = 0;
    }

    registerDataCallback(dataCallback: signalDataEventCallback) {
        this.dataCallback.push(dataCallback);
    }
    // getCurrentState(): Promise<any> {
    //     return new Promise(async (resolve, reject) => {

    //         this.mongoCommunicator.findOne(DB_COLLECTIONS.client_details, { type: "current_state" })
    //             .then((data) => {
    //                 resolve(data);
    //             })
    //             .catch((error) => {
    //                 reject(error);
    //             });


    //     })
    // }
   
    
    getLoadDataFromDB(filter: any): Promise<any> {
        return new Promise(async (resolve, reject) => {

            this.mongoCommunicator.find(DB_COLLECTIONS.client_details, filter, {}, true, {})
                .then((data: any[]) => {
                    resolve(data);
                })
                .catch((error) => {
                    reject(error);
                });


        })

    }

    initDataHander(dbUrl: string): Promise<any>{
        return new Promise((resolve, reject) => {
          
            this.mongoCommunicator.start(dbUrl, true)
            .then((data)=>{
                return this.getLoadDataFromDB({});
            })
            .then((data: any[])=>{
                data.forEach((obj)=>{
                    this.addNewClient(obj);
                })

            })
            .catch((error)=>{
                reject(error);
            })
        });

    }
    addNewClient(obj: any){
        obj.currentSeq = -1;
        let client = this.clientDetails.get(obj.accountID);
        if(client == null){
            client = new Client(obj);
            this.clientDetails.set(obj.accountID, client);
        }
        else{
            LOG.debug(`${obj.accountID} is already avilable`);
        }
    }

    getMongoCommunicator(){
       return this.mongoCommunicator; 
    }


    userAuthenticate(data: any){
        let client = this.clientDetails.get(data.accountID);
        

        if(client == null){
            if(data.accountType == 'real'){
                LOG.error("unknown user is recieved for real account");
                throw "unknown user is recieved for real account";
            }
            else{
                LOG.debug("unknown user is recieved for demo account");
                data.lastSeq = this.currentSeq;
                client = new Client(data);
                this.clientDetails.set(data.accountID, client);
            }
        }
        else{
            LOG.debug(`${JSON.stringify(data)} existing user connects with server.`);
        }
        client.onConnect();
        this.connectedClientCount += 1;
    }

    getSignal(accountID: string){

        let client: Client = this.clientDetails.get(accountID);
        if(client == null){
            LOG.error("unknown user is recieved for real account");
            throw "unknown user is recieved for real account";
    
        }
        else{
            if(client.getConnectionStatus() === false){
                throw "retry";
            }
            //let message = 
            
        }


    }

    setMessageCache(messageId: number, message: any){
        this.messageCache.set(messageId, message);
        this.currentSeq = messageId;
    }

    getMessageFromCache(messageId: number){
        // let message = this.messageCache.get(messageId);
        
        // return message;
    }

    userDisconnected(data: any){

        let client: Client= this.clientDetails.get(data.accountID);
        if(client != null){
            client.onDisConnect();
        }
       

    }


    




}
