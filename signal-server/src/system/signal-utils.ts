import { signalDataHandler } from "./signal-data-handler";

export class Utils {
    private static signalDataHander = undefined;

    static getDataHander(): signalDataHandler {
       return this.signalDataHander;
    }
    static initDataHander(instanceId: string, dbUrl: string): Promise<any>{
        return new Promise((resolve, reject) => {
            if(this.signalDataHander == undefined){
                this.signalDataHander = new signalDataHandler(instanceId);
            }
            this.signalDataHander.initDataHander(dbUrl)
            .then(()=>{
                resolve("ok");
            })
            .catch((error)=>{
                reject(error);
            })
        });


    }
}