// Copyright 2018 Yaala Labs (www.yaalalabs.com). All rights reserved.

import { Request, Response } from 'express';
import { ErrorHandler, ErrorType } from '../../utils/error-handler';
import { Utils } from '../../system/signal-utils';

export class OperatorController {

    getSignal(req: Request, res: Response) {
        const accountID = req.params.accountID;
        
        try {
            const signal = Utils.getDataHander().getSignal(accountID);
            res.status(200).send(signal);
        } catch (err) {
            if(err === "retry"){
                return ErrorHandler.sendError(res, ErrorType.Retry, err);
            }
            return ErrorHandler.sendError(res, ErrorType.SystemError, err);
        }

    }



}