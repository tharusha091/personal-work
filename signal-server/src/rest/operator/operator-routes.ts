// Copyright 2018 Yaala Labs (www.yaalalabs.com). All rights reserved.

import { OperatorController } from './operator-controller';

export class OperatorRoutes {
    operatorController: OperatorController = new OperatorController();

    configureRoutes(app): void {
        app.route('/signal/:accountID')
            .get(this.operatorController.getSignal.bind(this.operatorController));
       
    }
}