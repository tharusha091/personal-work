// Copyright 2018 Yaala Labs (www.yaalalabs.com). All rights reserved.

import { Request, Response } from 'express';
import { AdminController } from './admin-controller';

export class AdminRoutes {
    adminController: AdminController = new AdminController();

    configureRoutes(app): void {
        // Load signal data
        app.route('/admin/user')
            .post(this.adminController.addNewClient.bind(this.adminController));
        app.route('/admin/user')
            .put(this.adminController.updateClient.bind(this.adminController));
        app.route('/admin/user/authenticate')
            .post(this.adminController.userAuthenticate.bind(this.adminController));
        app.route('/admin/user/disconnect')
            .post(this.adminController.userDisconnected.bind(this.adminController));
        
    }
}