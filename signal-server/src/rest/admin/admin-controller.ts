// Copyright 2018 Yaala Labs (www.yaalalabs.com). All rights reserved.

import { Request, Response } from 'express';
import { ErrorHandler, ErrorType } from '../../utils/error-handler';
import { Utils } from '../../system/signal-utils';
import { LOG } from '../../utils/logger';


export class AdminController {

    addNewClient(req: Request, res: Response) {
        try {
            const data = req.params.body;
            Utils.getDataHander().addNewClient(data);
            res.status(200).send({status:'user is added'});
        } catch (error) {
            LOG.error(error);
            return ErrorHandler.sendError(res, ErrorType.SystemError, 'Failed to load signal data. error: ' + error);
            
        }
    }
    updateClient(req: Request, res: Response) {
        try {
            const data = req.params.body;
            Utils.getDataHander().addNewClient(data);
            res.status(200).send({status:'user is added'});
        } catch (error) {
            LOG.error(error);
            return ErrorHandler.sendError(res, ErrorType.SystemError, 'Failed to load signal data. error: ' + error);
            
        }
    }
    userAuthenticate(req: Request, res: Response) {
        
        try {
            const data = req.params.body;
            Utils.getDataHander().userAuthenticate(data);
            res.status(200).send({status:'user is added'});
        } catch (error) {
            LOG.error(error);
            return ErrorHandler.sendError(res, ErrorType.SystemError, 'Failed to load signal data. error: ' + error);
            
        }
       
    }
    userDisconnected(req: Request, res: Response) {
        
        try {
            const data = req.params.body;
            Utils.getDataHander().userDisconnected(data);
            res.status(200).send({status:'user is added'});
        } catch (error) {
            LOG.error(error);
            return ErrorHandler.sendError(res, ErrorType.SystemError, 'Failed to load signal data. error: ' + error);
            
        }
       
    }


}