// Copyright 2018 Yaala Labs (www.yaalalabs.com). All rights reserved.

import { Request, Response } from 'express';
import { AdminRoutes } from './admin/admin-routes';
import { OperatorRoutes } from './operator/operator-routes';

export class Routes {
    private adminRoutes: AdminRoutes = new AdminRoutes();
    private operatorRoutes: OperatorRoutes = new OperatorRoutes();

    public configureRoutes(app): void {
        app.route('/')
            .get((req: Request, res: Response) => {
                res.status(200).send({
                    message: 'lime-butler is working'
                })
            });

        this.configureInternalRoutes(app);
    }

    private configureInternalRoutes(app: any) {
        this.adminRoutes.configureRoutes(app);
        this.operatorRoutes.configureRoutes(app);
    }
}