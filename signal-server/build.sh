#!/bin/bash
# Copyright 2018 Yaala Labs (www.yaalalabs.com). All rights reserved.

set -e
base_tag=-"t signal-server"
use_docker=0

if [ ! -z $1 ]; then
    source_tag=$1
    shift
fi

while [ ! -z $1 ]; do
    if [ $1 == '--use-docker' ];then
        use_docker=1
    elif [ $1 == 'latest' ];then
        tags="$tags $base_tag"
    else
        tags="$tags $base_tag:$1"
    fi

    shift
done

if [ -z ${tags+x} ]; then
    tags=$base_tag
fi

if [ -z ${source_tag+x} ]; then
    DATE=`date '+%Y%m%d-%H%M'`
    source_tag=$DATE
fi

echo "Source tag $source_tag"

echo "building $tags"

path=`dirname $0`
pushd .

cd $path


./app_build_commands.sh


tar -ch . | docker build --build-arg VERSION_TAG=$source_tag --force-rm --compress $tags -

popd
