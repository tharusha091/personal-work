#!/bin/bash
# Copyright 2018 Yaala Labs (www.yaalalabs.com). All rights reserved.
# build the application

# when running in side the docker container as non root, HOME=/ which it cant access
if [ -f /.dockerenv ]; then
    echo "Application build executed inside docker";
    export HOME=/signal-server
else
    echo "Application build executed in host";
fi

rm -rf ./node_modules
#rm -rf ./package-lock.json

rm -rf ./lib

npm install
npm run build
